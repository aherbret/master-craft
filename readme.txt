Compilation:
A la racine du dossier du projet:
mkdir master-craft-build
cd master-craft-build
cmake ..
make
cd master-craft (important pour bien localiser le dossier assets)

#############################################################################################

Lancement :
Par défaut :
./master-craft_main
Cherche les fichiers images (textures, height map, etc.) dans ../../assets

#############################################################################################

EXEMPLE NOTABLE D'EXECUTION:
./master-craft/master-craft_main [CHEMIN VERS DOSSIER ASSETS] hMap1000t.bmp hMap1000Color.bmp

#############################################################################################

En indiquant les fichiers (height map, type de terrain):
./master-craft_main ~/Desktop/master-craft/assets hMap1000.bmp hMap1000Color.bmp

 --Contrôles clavier--
Direction :
z Déplacement avant
q Déplacement gauche
s Déplacement arrière
d Déplacement droite
Espace Saut/Haut
LCRTL Bas

Angles caméra : 
Flèche Haut   Rotation vers le haut
Flèche Gauche Rotation vers la gauche
Flèche Basse  Rotation vers le bas
Flèche Droite Rotation vers la droite
Souris

différent mode :
- noMouse Default false 
pas de capture des mouvement de la souris
Touche N

- isSun Default true
soleil fixe / nuit fixe (shift instantanné entre les modes jour/nuit)
Touche E

- fly Default false
permet de voler 
Touche F

- timeState Default false
cycle jour et nuit avec illumination dynamique
Touche T

Quitter :
ECHAP / Croix rouge en haut à droite

#############################################################################################

Options réalisées :
Animation de la lumière directionnelle de « jour »
Types supplémentaires de terrains, décors et personnages
Système de colision avec les murs et les décors
Système de gravité
Système de saut (pour le personnage)
2 types de NPC qui ont des comportements différents : oiseau et slime
Oiseau : déplacement sur toutes les directions, évite le sol
Slime : reste collé au sol, reste toujours sur la même platforme
Sortie du terrain possible en mode fly

Notes supplémentaires:
Certains arbres semblent flotter, mais cela s'explique par les chunks de la map qui ne sont pas chargés. Il suffit juste de s'approcher de l'objet pour s'apercevoir qu'il est en réalité bien placé sur la map.
