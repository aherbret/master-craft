#version 300 es

precision mediump float;
precision mediump sampler2DArray;


// Attributs de sommet
in vec3 vPosition_vs; // Position du sommet transformé dans l'espace View
in vec3 vNormal_vs; // Normale du sommet transformé dans l'espace View
in vec2 vTexCoords; // Coordonnées de texture du sommet
in float vTex;

uniform sampler2DArray uTextureArray;

uniform vec3 uAmbientLight;
uniform float uApplyLight;

uniform vec3 uKd;
uniform vec3 uKs;
uniform float uShininess;
uniform vec3 uLightDir_vs;
uniform vec3 uLightIntensity;
uniform vec3 uPosCam; // Coordonnées camera
uniform vec3 uCenter;

out vec3 fFragColor;

/*** Fonction ***/


vec3 lumiereDynamique(vec3 position_vs, vec3 normal_vs){

    vec3 w_zero = (-uCenter+uPosCam);
    vec3 w_i = (-uCenter+uLightDir_vs);
    vec3 halfVector = ((w_zero + w_i)/2.0);
    w_zero = normalize(w_zero);
    w_i = normalize(w_i);
    halfVector = normalize(halfVector);
    vec3 lum =  uLightIntensity * ( uKd * ( dot(w_i, normal_vs ) ) + uKs * ( pow( dot(halfVector, normal_vs), uShininess ) ) );

	if (lum.x < 0.0f) lum.x=0.0f;
	else if (lum.x > 0.0f) lum.x=lum.x;
	else lum.x=0.0f;
	
	if (lum.x < 0.0f) lum.x=0.0f;
	else if (lum.x > 0.0f) lum.x=lum.x;
	else lum.x=0.0f;

	if (lum.x < 0.0f) lum.x=0.0f;
	else if (lum.x > 0.0f) lum.x=lum.x;
	else lum.x=0.0f;
	return lum;
}

vec3 lumiereStatic(vec3 position_vs, vec3 normal_vs){

    vec3 w_zero = (uPosCam);
    vec3 Center = vec3(1,1,1);
    vec3 w_i = (uLightDir_vs);
    vec3 halfVector = ((w_zero + w_i)/2.0);
    w_zero = normalize(w_zero);
    w_i = normalize(w_i);
    halfVector = normalize(halfVector);


    vec3 lum =  uLightIntensity * ( uKd * ( dot(w_i, normal_vs ) ) + uKs * ( pow( dot(halfVector, normal_vs), uShininess ) ) );
	if (lum.x < -0.5f) lum.x=-0.5f;
	else if (lum.x  > 0.0f) lum.x=lum.x;
	else lum.x=0.0f;

	if (lum.y < -0.5f) lum.y=-0.5f;
	else if (lum.y  > 0.0f) lum.y=lum.y;
	else lum.y=0.0f;

	if (lum.z < -0.5f) lum.z=-0.5f;
	else if (lum.z  > 0.0f) lum.z=lum.z;
	else lum.z=0.0f;
	return lum;
}




void main() {
    if (uApplyLight <= 0.0){
		fFragColor = texture(uTextureArray, vec3(vTexCoords,vTex)).rgb;
	}
    else if (uApplyLight == 2.0){
		fFragColor = texture(uTextureArray, vec3(vTexCoords,vTex)).rgb + uAmbientLight;
	}
	else {
	    vec3 Lumiere = lumiereStatic(vPosition_vs, vNormal_vs);
		fFragColor = texture(uTextureArray, vec3(vTexCoords,vTex)).rgb + Lumiere + uAmbientLight;
		//fFragColor = Lumiere + uAmbientLight;
		//fFragColor = vPosition_vs;
	}
};
