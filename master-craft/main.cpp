#include <glimac/SDLWindowManager.hpp>
#include <GL/glew.h>
#include <iostream>
#include <glimac/Program.hpp>
#include <glimac/FilePath.hpp>
#include <glimac/Cube.hpp>
#include <glimac/common.hpp>
#include <glimac/Image.hpp>
#include <glimac/FreeFlyCamera.hpp>
#include <glimac/LightBlock.hpp>
#include <glimac/ElementForm.hpp>
#include <glimac/Npc.hpp>
#include <glimac/DeepMap.hpp>
#include <glimac/Functions.hpp>
#include <masterCraft/ModelAnim.hpp>
#include <masterCraft/ModelAI.hpp>
#include <masterCraft/ModelDeep.hpp>
#include <glimac/Chunck.hpp>
#include <chrono>
#include <algorithm>
#include <map>
#include <tuple>
#include <ctime>
#include <list>
using namespace glimac;
using namespace masterCraft;

/***********  Initialisation Variable  ************/

constexpr int VERTEX_ATTR_POSITION = 0; // nom de l'index position
constexpr int VERTEX_ATTR_NORMAL = 1; // nom de l'index Normal
constexpr int VERTEX_ATTR_TEXCOORDS = 2; // nom de l'index TexCoord
constexpr int VERTEX_ATTR_TEXTURE = 3; // nom de l'index Texture

/***********  Initialisation Variable  ************/
int main(int argc, char** argv) {

    std::string pathToImages = "../../assets";
    std::string pathToMap = (pathToImages + "/map/" + "hMap100.bmp");
    std::string pathToMapColor = (pathToImages + "/map/" + "hMap100Color.bmp");

    if (argc == 4 ){
      pathToImages = argv[1];
      pathToMap = (pathToImages + "/map/" + argv[2]);
      pathToMapColor = (pathToImages + "/map/" + argv[3]);
    }




    // Initialize SDL and open a window
    glm::ivec2 WindowSize(1200,600);

    //SDLWindowManager windowManager(WindowSize.x, WindowSize.y, "GLImac");
    SDLWindowManager windowManager("GLImac");
    WindowSize = SDLWindowManager::screenSize();

    // Initialize glew for OpenGL3+ support
    GLenum glewInitError = glewInit();
    if(GLEW_OK != glewInitError) {
        std::cerr << glewGetErrorString(glewInitError) << std::endl;
        return EXIT_FAILURE;
    }

    SDL_WM_GrabInput( SDL_GRAB_ON );

    /*   Declaration Variable    */
    FilePath applicationPath(argv[0]);
    Program program = loadProgram(applicationPath.dirPath() + "shaders/3D.vs.glsl",
                                  applicationPath.dirPath() + "shaders/Tex3D.fs.glsl");
    program.use();

    glEnable(GL_DEPTH_TEST);
    FreeFlyCamera myCamera;

    //std::cout << "OpenGL Version : " << glGetString(GL_VERSION) << std::endl;
    //std::cout << "GLEW Version : " << glewGetString(GLEW_VERSION) << std::endl;

    /*********************************
     * HERE SHOULD COME THE INITIALIZATION CODE
     *********************************/
    //std::srand(0); // use current time as seed for random generator
    std::srand(std::time(0)); // use current time as seed for random generator
    glm::mat4 ProjMatrix,MVMatrix,NormalMatrix;
    ProjMatrix = glm::perspective(glm::radians(70.),((double)WindowSize.x/(double)WindowSize.y),0.1 ,300.);
    MVMatrix = glm::translate(MVMatrix ,glm::vec3(0.,0.,-5.));
    NormalMatrix = glm::transpose(glm::inverse(MVMatrix));

    /******** uniforme Variable *******/
    auto uMap = initUniformeVar(program);

    /******** Texture *********/
    GLuint *texture;
    GLuint *arrayTexture;
    auto uNameText = initUniformTex(texture, arrayTexture, pathToImages);

    /****** Sky sun and moon ******/
    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER,vbo);
    Cube sky(0,1,2); // Front Up Down
    glBufferData(GL_ARRAY_BUFFER, sky.getVertexCount()*sizeof(ShapeVertex) , sky.getDataPointer(),GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER,0);
    GLuint vao;
    glGenVertexArrays(1,&vao);
    glBindVertexArray(vao);
    glEnableVertexAttribArray(VERTEX_ATTR_POSITION);
    glEnableVertexAttribArray(VERTEX_ATTR_NORMAL);
    glEnableVertexAttribArray(VERTEX_ATTR_TEXCOORDS);
    glEnableVertexAttribArray(VERTEX_ATTR_TEXTURE);
    glBindBuffer(GL_ARRAY_BUFFER,vbo);
    glVertexAttribPointer(VERTEX_ATTR_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), 0);
    glVertexAttribPointer(VERTEX_ATTR_NORMAL, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)(sizeof(glm::vec3)));
    glVertexAttribPointer(VERTEX_ATTR_TEXCOORDS, 2, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)(sizeof(glm::vec3)+sizeof(glm::vec3)));
    glVertexAttribPointer(VERTEX_ATTR_TEXTURE, 1, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)(sizeof(glm::vec3)+sizeof(glm::vec3)+sizeof(glm::vec2)));
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindVertexArray(0);

    /*** Matrice Sky ***/
    glm::mat4 MVMatrixSky = glm::scale(glm::mat4(1), glm::vec3(50, 50, 50)); // Scale

    float PhiInit = myCamera.getPhi();
  float ThetaInit = myCamera.getTheta();

    /*** Lum ***/
    glm::vec3 LightDir;
    glm::vec3 LightIntensity = glm::vec3(0.1,0.1,0.1);;
    glm::vec3 AmbientLight = glm::vec3(-0.1,-0.1,-0.1);

    LightBlock lightElm(glm::vec3(0.1, 0.1, 0.1),glm::vec3(0.1, 0.1, 0.1),0.5);
    LightBlock lightMetal(glm::vec3(0.1, 0.1, 0.1),glm::vec3(0.3, 0.3, 0.3),2);
    LightBlock lightTree(glm::vec3(0.1, 0.1, 0.1),glm::vec3(0.1, 0.1, 0.1),1);
    LightSource lightSrc(glm::vec3(0,0,0), glm::vec3(0,0,0), glm::vec3(0,0,0));

    /*** map ***/

    auto heightMapImg = loadImage(pathToMap);
    auto heightMapImgColor = loadImage(pathToMapColor);

    if(!heightMapImg) {
        std::cout << "image is not loaded" << std::endl;
    }
    int width = heightMapImg->getWidth();
    int hight = heightMapImg->getHeight();
    if (width < 100 || hight < 100){
      std::cout  << "Picture too small" << std::endl;
      return 0; 
     }
     if (width != heightMapImgColor->getWidth() || hight != heightMapImgColor->getHeight()){
      std::cout  << "colorMap and heightMap wrong size" << std::endl;
      return 0;
     }
    auto hMap = loadHeightMap(std::move(heightMapImg));
    auto mapColor = loadColoredMap(std::move(heightMapImgColor));

    /*** Define bloque color type ***/
    std::vector<glm::vec3> typeCubeColor;
    std::vector<Cube> typeCube;

    auto dirtColor = glm::vec3(0,255,0);
    Cube dirt(3,0,2);
    typeCubeColor.push_back(dirtColor);// dirt
    typeCube.push_back(dirt);
    typeCubeColor.push_back(dirtColor);// dirt
    typeCube.push_back(Cube(2,2,2));

    auto sandColor = glm::vec3(255,255,0);
    Cube sand(18, 18, 18);
    typeCubeColor.push_back(glm::vec3(255,255,0)); //sand
    typeCube.push_back(sand);
    typeCubeColor.push_back(glm::vec3(255,255,0)); //sand
    typeCube.push_back(sand);

    auto montainsColor = glm::vec3(255,0,0);
    Cube montains(1, 1, 1);
    typeCubeColor.push_back(glm::vec3(255,0,0)); // montains
    typeCube.push_back(montains);
    typeCubeColor.push_back(glm::vec3(255,0,0)); // montains
    typeCube.push_back(montains);
    
    Cube snow(67, 67, 67);
    typeCubeColor.push_back(glm::vec3(0,0,255)); // Ice
    typeCube.push_back(snow);
    typeCubeColor.push_back(glm::vec3(0,0,255)); // Ice
    typeCube.push_back(snow);




    /******  Creation deep   *****/

    DeepMap deepMap(width,hight);
    deepMap.generateDeepFromImage(hMap,width,hight);

    /*** Deco ***/

    //std::cout << "Data  " << width << " " << hight<< std::endl;

    std::vector<std::tuple<std::shared_ptr<ElementForm>, glm::mat4>> mapDeco;

    auto appleTree = ElementForm::CreateElementForm(0, uNameText["MapAtlas"], &lightTree, bindTextureApple, createApple, getVertexCountApple, VERTEX_ATTR_POSITION, VERTEX_ATTR_NORMAL, VERTEX_ATTR_TEXCOORDS, VERTEX_ATTR_TEXTURE);    
    auto sapin = ElementForm::CreateElementForm(0, uNameText["MapAtlas"], &lightTree, bindTextureSapin, createSapin, getVertexCountSapin, VERTEX_ATTR_POSITION, VERTEX_ATTR_NORMAL, VERTEX_ATTR_TEXCOORDS, VERTEX_ATTR_TEXTURE);    
    auto cactus = ElementForm::CreateElementForm(0, uNameText["MapAtlas"], &lightTree, bindTextureCactus, createCactus, getVertexCountCactus, VERTEX_ATTR_POSITION, VERTEX_ATTR_NORMAL, VERTEX_ATTR_TEXCOORDS, VERTEX_ATTR_TEXTURE);    
    auto appleDeepMap = generateDeepApple();
    auto sapinDeepMap = generateDeepSapin();
    auto cactusDeepMap = generateDeepCactus();
    // Tree  ## Herbe
    int errGen = 0;
    for (auto i = 0; i< 10 ; i++){
        int randX = int((width-5)/2.)-((std::rand()%(width -5)));
        int randZ = int((hight-5)/2.)-((std::rand()%(hight -5)));
        int y = deepMap.getGround(randX*2, randZ*2)+2; // coord y dans la map
        //std::cout << " Info " << mapColor[indexPos(randX, randZ ,width ,hight)] << " " << dirtColor << std::endl;
        while (!deepMap.isEmpty(randX*2,y , randZ*2) ||
          (mapColor[indexPos(randX, randZ ,width ,hight)] != dirtColor)){  
             errGen++;
             if (errGen > 100) break;
             //std::cout << "Colision Apple "<< randX << " " << randZ << " " << mapColor[indexPos(randX, randZ ,width ,hight)] << " Err nb " << errGen << std::endl;
             randX = int((width-5)/2.)-((std::rand()%(width -5)));
             randZ = int((hight-5)/2.)-((std::rand()%(hight -5)));
             y = (deepMap.getGround(randX*2, randZ*2)+2);

        }
        if (errGen > 100) break;
        auto randposMap = glm::translate(glm::mat4(1), glm::vec3(randX,(y/2.)+1.,randZ));
        auto randpos = glm::translate(glm::mat4(1), glm::vec3(randX*2,y,randZ*2));
        deepMap.addDeep(*appleDeepMap, randposMap);
        mapDeco.push_back(std::make_tuple(appleTree, randpos));   
    }   

    
    //Sapin ## montains
    errGen = 0;
    for (auto i = 0; i< 10 ; i++){
        int randX = int((width-5)/2.)-((std::rand()%(width -5)));
        int randZ = int((hight-5)/2.)-((std::rand()%(hight -5)));
        int y = (deepMap.getGround(randX*2, randZ*2)+2);
        while (!deepMap.isEmpty(randX*2,y , randZ*2) ||
          mapColor[indexPos(randX, randZ ,width ,hight)] != montainsColor){  
             errGen++;
             if (errGen > 100) break;
             //std::cout << "Colision Sapin "<< randX << " " << randZ << " Err nb" << errGen << std::endl;
             randX = int((width-5)/2.)-((std::rand()%(width -5)));
             randZ = int((hight-5)/2.)-((std::rand()%(hight -5)));
             y = (deepMap.getGround(randX*2, randZ*2)+2);

        }
        if (errGen > 100) break;
        auto randposMap = glm::translate(glm::mat4(1), glm::vec3(randX,(y/2.)+1.,randZ));
        auto randpos = glm::translate(glm::mat4(1), glm::vec3(randX*2,y,randZ*2));
        deepMap.addDeep(*sapinDeepMap, randposMap);
        mapDeco.push_back(std::make_tuple(sapin, randpos));
        
    }    
    // Cactus ## sable
    errGen = 0;
    for (auto i = 0; i< 20 ; i++){
        int randX = int((width-5)/2.)-((std::rand()%(width -5)));
        int randZ = int((hight-5)/2.)-((std::rand()%(hight -5)));
        int y = (deepMap.getGround(randX*2, randZ*2)+2);
        while (!deepMap.isEmpty(randX*2,y , randZ*2) ||
          mapColor[indexPos(randX, randZ ,width ,hight)] != sandColor){  
             errGen++;
             if (errGen > 100) break;
             //std::cout << "Colision Cactus "<< randX << " " << randZ << " Err nb" << errGen << std::endl;
             randX = int((width-5)/2.)-((std::rand()%(width -5)));
             randZ = int((hight-5)/2.)-((std::rand()%(hight -5)));
             y = (deepMap.getGround(randX*2, randZ*2)+2);

        }
        if (errGen > 100) break;
        auto randposMap = glm::translate(glm::mat4(1), glm::vec3(randX,(y/2.)+1.,randZ));
        auto randpos = glm::translate(glm::mat4(1), glm::vec3(randX*2,y,randZ*2));
        deepMap.addDeep(*cactusDeepMap, randposMap);
        mapDeco.push_back(std::make_tuple(cactus, randpos));
    }    
    /*** AI ***/
    int y = 0;
    auto birdTntNpc = Npc(generateCiblePosBird, generateDeplacementBird, generateNextPosBird, generateDodgeAIBird);
    birdTntNpc.setElementForm(10, uNameText["MapAtlas"], &lightMetal, bindTextureTntBird, createTntBird, getVertexCountTntBird, VERTEX_ATTR_POSITION, VERTEX_ATTR_NORMAL, VERTEX_ATTR_TEXCOORDS, VERTEX_ATTR_TEXTURE);
    // Bird
    y = (deepMap.getGround(0, 0)+10);
    subNpc AI1(0.1,0.05,glm::vec3(0,y,2),glm::vec3(0,y,2), glm::vec3(0,0,0));
    y = (deepMap.getGround(2, 0)+10);
    subNpc AI2(0.2,0.05,glm::vec3(2,y,0),glm::vec3(2,y,0), glm::vec3(0,0,0));
    y = (deepMap.getGround(2, 2)+10);
    subNpc AI3(0.1,0.05,glm::vec3(2,y,2),glm::vec3(2,y,2), glm::vec3(0,0,0));


    auto slimeNpc = Npc(generateCiblePosCitrouSlime, generateDeplacementCitrouSlime, generateNextPosCitrouSlime, generateDodgeAICitrouSlime);
    slimeNpc.setElementForm(10, uNameText["MapAtlas"], &lightElm, bindTextureCitrouSlime, createCitrouSlime, getVertexCountCitrouSlime, VERTEX_ATTR_POSITION, VERTEX_ATTR_NORMAL, VERTEX_ATTR_TEXCOORDS, VERTEX_ATTR_TEXTURE);
    // Slime
    y = (deepMap.getGround(0, 0))+2;
    subNpc AI4(0.05,0.05,glm::vec3(0,y,0),glm::vec3(0,y,0), glm::vec3(0,0,0));
    y = (deepMap.getGround(2, 0))+2;
    subNpc AI5(0.05,0.05,glm::vec3(2,y,0),glm::vec3(2,y,0), glm::vec3(0,0,0));
    y = (deepMap.getGround(2, 2))+2;
    subNpc AI6(0.05,0.05,glm::vec3(2,y,2),glm::vec3(2,y,2), glm::vec3(0,0,0));
    y = (deepMap.getGround(30, 30))+2;
    subNpc AI7(0.05,0.05,glm::vec3(30,y,30),glm::vec3(30,y,30), glm::vec3(0,0,0));


    myCamera.setXYZ(glm::vec3(0,(deepMap.getGround(0,0)+4),0));

    /*Size chunck */
    int chunckX = 32;
    int chunckZ = 32;

    int nbChunckX = (int)((float)width/chunckX);
    int nbChunckZ = (int)((float)hight/chunckZ);
    int sizeData = 0;

    int lastGroundY = 0;
    // Bool State
    bool timeState = false;
    float timeSun = 3.0;
    bool keyIsAlreadyPressed=false;
    bool noMouse=false;
    bool fly=false;
    bool isSun = true;

    // Application loop:
    bool done = false;
    bool jump = false;
    std::list<std::tuple<std::shared_ptr<ElementForm>, glm::mat4, glm::vec3>> lstChunckCache;
    glm::ivec2 oldMousePos = windowManager.getMousePosition();
    while(!done) {

        auto start = std::chrono::high_resolution_clock::now();
        MVMatrix = myCamera.getViewMatrix();
        // Event loop:
        SDL_Event e;
        while(windowManager.pollEvent(e)) {
            if(e.type == SDL_QUIT) {
                done = true; // Leave the loop after this iteration
            }
        }
        /*** Gestion des touches***/
        auto oldPos = myCamera.getPos();
        if (fly){
          if(windowManager.isKeyPressed(SDLK_z)) myCamera.moveFrontFly(0.5);
          if(windowManager.isKeyPressed(SDLK_s)) myCamera.moveFrontFly(-0.5);
          if(windowManager.isKeyPressed(SDLK_q)) myCamera.moveLeftFly(0.5);
          if(windowManager.isKeyPressed(SDLK_d)) myCamera.moveLeftFly(-0.5);
        }
        else {
          glm::vec3 posCam;
          bool useFront = false;
          bool useBack = false;
          bool useLeft = false;
          bool useRight = false;
          //// Colision bord image
          // up
          if(windowManager.isKeyPressed(SDLK_z)) myCamera.moveFront(0.3);
          posCam = myCamera.getPos();
          if (posCam.x > width || posCam.x < (-width) || posCam.z > hight || posCam.z < (-hight)){
            myCamera.moveFront(-0.3);
          }
          // down
          if(windowManager.isKeyPressed(SDLK_s)) myCamera.moveFront(-0.3);
          posCam = myCamera.getPos();
          if (posCam.x > width || posCam.x < (-width) || posCam.z > hight || posCam.z < (-hight)){
            myCamera.moveFront(0.3);
          }
          // left
          if(windowManager.isKeyPressed(SDLK_q)) myCamera.moveLeft(0.3);
          posCam = myCamera.getPos();
          if (posCam.x > width || posCam.x < (-width) || posCam.z > hight || posCam.z < (-hight)){
            myCamera.moveLeft(-0.3);
          }
          //right
          if(windowManager.isKeyPressed(SDLK_d)) myCamera.moveLeft(-0.3); 
          posCam = myCamera.getPos();
          if (posCam.x > width || posCam.x < (-width) || posCam.z > hight || posCam.z < (-hight)){
            myCamera.moveLeft(0.3);
          }
        }
        if(windowManager.isKeyPressed(SDLK_DOWN)) myCamera.rotateUp(-1.0);
        if(windowManager.isKeyPressed(SDLK_UP)) myCamera.rotateUp(1.0);
        if(windowManager.isKeyPressed(SDLK_LEFT)) myCamera.rotateLeft(1.0);
        if(windowManager.isKeyPressed(SDLK_RIGHT)) myCamera.rotateLeft(-1.0);

        if(windowManager.isKeyPressed(SDLK_ESCAPE)) done = true;
        if(windowManager.isKeyPressed(SDLK_e)) {
          if (!keyIsAlreadyPressed){
              timeState = false;
              if (timeSun < 3.5) timeSun=15.6;
              else timeSun=3.0;
              keyIsAlreadyPressed = true;
            }
          }
        if(windowManager.isKeyPressed(SDLK_t)){
            if (!keyIsAlreadyPressed){
              timeState = (!timeState);
              keyIsAlreadyPressed = true;
            }
        }
        if(windowManager.isKeyPressed(SDLK_n)){
            if (!keyIsAlreadyPressed){
              noMouse = (!noMouse);
              keyIsAlreadyPressed = true;
            }
        }
        if(windowManager.isKeyPressed(SDLK_f)){
            if (!keyIsAlreadyPressed){
              fly = (!fly);
              keyIsAlreadyPressed = true;
            }
        }
        if (!noMouse){
          glm::ivec2 mousePos = oldMousePos - windowManager.getMousePosition();
          myCamera.rotateLeft(mousePos.x/4);
          myCamera.rotateUp(mousePos.y/3);
          oldMousePos = windowManager.getMousePosition();

          //SDL_WarpMouse(WindowSize.x/2, WindowSize.y/2);          
        }


        if (fly){
          if(windowManager.isKeyPressed(SDLK_SPACE)) myCamera.moveUpFly(0.5);
          if(windowManager.isKeyPressed(SDLK_LCTRL)) myCamera.moveUpFly(-0.5);
        }

        if ( e.type == SDL_KEYUP )
        {
           keyIsAlreadyPressed=false;
        }

        glm::vec3  posCam = myCamera.getPos();
        /*Modèle simple Lumiere Jour / nuit*/

        float time = windowManager.getTime()/2.;
        if (timeState){
          timeSun = time;
        }

        /*** Gestion Colision block décord***/
       if (!fly){
        glm::vec3 tmpPos = posCam;
        try{
          if (!deepMap.isEmptyPlayer(round(tmpPos.x),round(tmpPos.y),round(tmpPos.z)) 
            || !deepMap.isEmptyPlayer(round(tmpPos.x),round(tmpPos.y-2),round(tmpPos.z))){
            myCamera.setXYZ(oldPos);
          }
        }
        catch(...){
          //std::cout << "Player out of the map pas de correction possible" << std::endl;
        }
       }


        /*** Gestion de la gravité du joueur et colision avec le terrain***/
        /* Set d'information :
         *  Gravité : 0.2 pixel /tour
         *  Pos joueur : Ground + 2 
         * 
         */
        if (!fly){
          auto y = 0;
          auto gravite = glm::vec3(0,-0.3,0);
          try {
            auto posX = posCam.x;
            auto posZ = posCam.z;
            double distParRapportSol = 0;
            y  = deepMap.getGround((int)(posCam.x), (int)(posCam.z));
            if (posCam.x < 0 && posCam.z > 0 ){
              posX = posCam.x-2;
              posZ = posCam.z+2;
              y  = deepMap.getGround((int)(posCam.x-2), (int)(posCam.z+2));
            }
            else if (posCam.x > 0 && posCam.z >0 ){
              posX = posCam.x+2;
              posZ = posCam.z+2;
              y  = deepMap.getGround((int)(posCam.x+2), (int)(posCam.z+2));
            }
            
            distParRapportSol = (int)posCam.y - y;
            // Jump
            if(windowManager.isKeyPressed(SDLK_SPACE) && jump){
                if (distParRapportSol-(lastGroundY - y) >= 16.){
                  jump = false;
                }
                if (deepMap.isEmptyPlayer(posCam.x, posCam.y+0.8, posCam.z)){
                  myCamera.moveUp(0.8);
                }
                else {
                  jump = false;                  
                }

            } 

            // Gravite
            if (distParRapportSol > 4. ){
                if (deepMap.isEmptyPlayer(posCam.x, posCam.y-gravite.y-4, posCam.z)){
                  myCamera.moveUp(gravite.y);                  
                }
                else {            
                  lastGroundY = posCam.y-4;
                  distParRapportSol = 4;
                }

            }            
            if (distParRapportSol <=4 && distParRapportSol > 2){
              lastGroundY = posCam.y-4;
              jump = true;
            }


            // Jump cam or invalid move
            if (distParRapportSol <= 2. ){
                //myCamera.moveUp(y+4-posCam.y);
              myCamera.setXYZ(oldPos);
            }

            //std::cout << distParRapportSol << " " << y << std::endl;
          }
          catch(...){
            //std::cout << "Player out of the map pas de correction possible" << std::endl;
          }
          
        }
        //std::cout << posCam  << std::endl;

/***********************************************************************************************************************/
      /***** Gestion des chuncks *****/

      if (lstChunckCache.size() > 100){
        throw std::length_error("Maybe too much element"); 
      }
      //Distance par rapport au centre du chunck
      int distMaxChunck = 150;
      int distMaxAffichageChunck = distMaxChunck * 2;
      /**** Test si le cache de chunck doit étre désalouer****/
      auto lamdaIsTooFar = [posCam, distMaxChunck, distMaxAffichageChunck,chunckX,chunckZ](const std::tuple<std::shared_ptr<ElementForm>, glm::mat4, glm::vec3>& value){
        if (glm::distance(std::get<2>(value)*glm::vec3(chunckX,0,chunckZ)*glm::vec3(2,2,2), glm::vec3(posCam.x,0,posCam.z)) >  (distMaxAffichageChunck+distMaxChunck) ){//marge de sécurite
            //std::cout << "isRemove " << std::get<2>(value)*glm::vec3(chunckX,0,chunckZ)*glm::vec3(2,2,2) << posCam << std::endl;
            return true;
        }
        return false;
      };
      lstChunckCache.remove_if(lamdaIsTooFar);
      /**** to load ****/
      auto lamdaIsAlreadyCharged = [lstChunckCache](const glm::vec3 &value){
      
        for (auto i : lstChunckCache){
          if (std::get<2>(i) == value){
              return true;
            }
          }
          return false;
        };

      auto posDataHMap = glm::vec3((int)((posCam.x/2.)/chunckX),0,(int)((posCam.z/2.)/chunckZ));
      //std::cout << "Pos Cam in chunck" << posDataHMap << std::endl;
      std::vector<glm::vec3> toLoad;
      // load 9 block
      
      for (auto i = -3 ; i< 3; i++ ){
        for (auto j = -3 ; j< 3; j++ ){
            toLoad.push_back(glm::vec3(posDataHMap.x+i,0,posDataHMap.z+j));
        }
      }

      toLoad.erase(
        std::remove_if(toLoad.begin(), toLoad.end(), lamdaIsAlreadyCharged),toLoad.end()
        );
      /**** load ****/
      for (auto posData : toLoad){
          //if (lstChunckCache.size()<48){ // 0.1 s pour charger 1 block 
            auto chunk = Chunck::generateMapElementForm(deepMap, mapColor, typeCubeColor, typeCube, posData.x,posData.z, chunckX, chunckZ, uNameText["MapAtlas"], &lightElm, VERTEX_ATTR_POSITION, VERTEX_ATTR_NORMAL, VERTEX_ATTR_TEXCOORDS, VERTEX_ATTR_TEXTURE);
            auto centre = glm::vec3(posData.x*chunckX, 0, posData.z*chunckZ);
            auto pos = glm::translate(glm::mat4(1) ,glm::vec3(centre.x*2,0,centre.z*2));
            lstChunckCache.push_back(std::make_tuple(chunk,pos,posData)); 
      //} 
    } 
/**********************************************************************************************************************/


        /*** Gestion des position des astres***/
        glm::mat4 MVMatrixSun = glm::rotate(glm::mat4(1), timeSun/4 , glm::vec3(1, 0, 0)); // Scale
        MVMatrixSun = glm::translate(MVMatrixSun, glm::vec3(0,0,-200));
        MVMatrixSun = glm::scale(MVMatrixSun, glm::vec3(10, 10, 0.5)); // Scale

        glm::mat4 MVMatrixMoon = glm::rotate(glm::mat4(1), timeSun/4 , glm::vec3(1, 0, 0)); // Scale
        MVMatrixMoon = glm::translate(MVMatrixMoon, glm::vec3(0,0,200));
        MVMatrixMoon = glm::scale(MVMatrixMoon, glm::vec3(10, 10, 0.5)); // Scale

        /* Gestion de la lumiere du soleil*/
        if ((int)timeSun%24 <12){
          lightSrc.setLightDir_vs(glm::vec3(MVMatrixSun[3].x, MVMatrixSun[3].y, MVMatrixSun[3].z));
          lightSrc.setAmbientLight(glm::vec3(0,0,0));
          lightSrc.setLightIntensity(glm::vec3(1.1,1.1,1.1));
          isSun = true;
        }
        else {
          lightSrc.setLightDir_vs(glm::vec3(MVMatrixMoon[3].x, MVMatrixMoon[3].y, MVMatrixMoon[3].z));
          lightSrc.setAmbientLight(glm::vec3(-0.6,-0.6,-0.6));
          lightSrc.setLightIntensity(glm::vec3(1.0,1.0,1.0));
          isSun = false;
        }

        /*********************************
         * HERE SHOULD COME THE RENDERING CODE
         *********************************/


        /*** Clean windows ***/
         glClearColor(0.2,0.2,0.2,0.0);
         glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

         /*** Draw sky ***/
         float Phi = myCamera.getPhi();
         float Theta = myCamera.getTheta();
         glm::mat4 ConstDist = glm::rotate(glm::mat4(1),-Theta+ThetaInit, glm::vec3(1,0,0));
         ConstDist = glm::rotate(ConstDist,-Phi+PhiInit, glm::vec3(0,1,0));

         glUniformMatrix4fv((*uMap)["uMVPMatrix"], 1,GL_FALSE, glm::value_ptr(ProjMatrix*ConstDist * MVMatrixSky));
         glUniformMatrix4fv((*uMap)["uMVMatrix"], 1,GL_FALSE, glm::value_ptr(ConstDist * MVMatrixSky));
         glUniformMatrix4fv((*uMap)["uNormalMatrix"], 1,GL_FALSE, glm::value_ptr(NormalMatrix));
         glUniform1i((*uMap)["uTextureArray"], 0);
         lightSrc.uSendAmbientLight(uMap);
         glUniform1f((*uMap)["uApplyLight"], 2);

         glDepthMask(GL_FALSE);
         glBindVertexArray(vao);
         if(isSun){
             glBindTexture(GL_TEXTURE_2D_ARRAY, uNameText["Sky"]);
         }
         else {
             glBindTexture(GL_TEXTURE_2D_ARRAY, uNameText["SkyNight"]);
         }
         glDrawArrays(GL_TRIANGLES, 0, sky.getVertexCount());
         glBindTexture(GL_TEXTURE_2D_ARRAY, 0);

         glBindVertexArray(0);
         glDepthMask(GL_TRUE);

         /*** Draw sky ***/
         /*** Draw sun ***/
         glUniformMatrix4fv((*uMap)["uMVPMatrix"], 1,GL_FALSE, glm::value_ptr(ProjMatrix * ConstDist * MVMatrixSun));
         glUniformMatrix4fv((*uMap)["uMVMatrix"], 1,GL_FALSE, glm::value_ptr(ConstDist * MVMatrixSun));
         glUniformMatrix4fv((*uMap)["uNormalMatrix"], 1,GL_FALSE, glm::value_ptr(NormalMatrix));
         glUniform1i((*uMap)["uTextureArray"], 0);
         glUniform1f((*uMap)["uApplyLight"], false);

         glBindVertexArray(vao);
         glBindTexture(GL_TEXTURE_2D_ARRAY, uNameText["Sun"]);
         glDrawArrays(GL_TRIANGLES, 0, sky.getVertexCount());
         glBindTexture(GL_TEXTURE_2D_ARRAY, 0);

         glBindVertexArray(0);
         /*** Draw sun ***/
         /*** Draw Moon ***/
         glUniformMatrix4fv((*uMap)["uMVPMatrix"], 1,GL_FALSE, glm::value_ptr(ProjMatrix * ConstDist * MVMatrixMoon));
         glUniformMatrix4fv((*uMap)["uMVMatrix"], 1,GL_FALSE, glm::value_ptr(ConstDist * MVMatrixMoon));
         glUniformMatrix4fv((*uMap)["uNormalMatrix"], 1,GL_FALSE, glm::value_ptr(NormalMatrix));
         glUniform1i((*uMap)["uTextureArray"], 0);
         glUniform1f((*uMap)["uApplyLight"], false);

         glBindVertexArray(vao);
         glBindTexture(GL_TEXTURE_2D_ARRAY, uNameText["Moon"]);
         glDrawArrays(GL_TRIANGLES, 0, sky.getVertexCount());
         glBindTexture(GL_TEXTURE_2D_ARRAY, 0);

         glBindVertexArray(0);
         /*** Draw Moon ***/




         /***********************************************************************/
          //sharePtr ElmForm birdTnt
         
         birdTntNpc.updateCible(AI1, deepMap);
         birdTntNpc.updateCible(AI2, deepMap);
         birdTntNpc.updateCible(AI3, deepMap);

         slimeNpc.updateCible(AI4, deepMap);
         slimeNpc.updateCible(AI5, deepMap);
         slimeNpc.updateCible(AI6, deepMap);
         slimeNpc.updateCible(AI7, deepMap);

        /******* AI ******/
        birdTntNpc.uDraw(time, uMap, posCam, ProjMatrix, MVMatrix, NormalMatrix, lightSrc,AI1);
        birdTntNpc.uDraw(time, uMap, posCam, ProjMatrix, MVMatrix, NormalMatrix, lightSrc,AI2);
        birdTntNpc.uDraw(time, uMap, posCam, ProjMatrix, MVMatrix, NormalMatrix, lightSrc,AI3);

        slimeNpc.uDraw(time, uMap, posCam, ProjMatrix, MVMatrix, NormalMatrix, lightSrc,AI4);
        slimeNpc.uDraw(time, uMap, posCam, ProjMatrix, MVMatrix, NormalMatrix, lightSrc,AI5);
        slimeNpc.uDraw(time, uMap, posCam, ProjMatrix, MVMatrix, NormalMatrix, lightSrc,AI6);
        slimeNpc.uDraw(time, uMap, posCam, ProjMatrix, MVMatrix, NormalMatrix, lightSrc,AI7);

         /***********************************************************************/

        /**** Deco ****/
        for (auto i : mapDeco){
            std::get<0>(i)->uDraw(time, uMap, posCam, ProjMatrix, MVMatrix, NormalMatrix, std::get<1>(i)  ,lightSrc);
        }

        /**** Affichage des block du monde ****/
        for (auto i : lstChunckCache){
            /**** Affichage des blocs devant l'utilisateur et a coté ****/

            std::get<0>(i)->uDraw(time, uMap, posCam, ProjMatrix, MVMatrix, NormalMatrix, std::get<1>(i)  ,lightSrc);  
            //std::cout << " Center Block " << centerBlock << " poscam " << posCam << std::endl;

        }

      auto finish = std::chrono::high_resolution_clock::now();
      std::chrono::duration<double> elapsed = finish - start;
      //std::cout << "Elapsed time: " << elapsed.count() << " s\n";
        // Update the display
        windowManager.swapBuffers();
    }

    //free memoire
    glDeleteBuffers(1,&vbo);
    glDeleteVertexArrays(1,&vao);
    freeTex(texture, arrayTexture);


    return EXIT_SUCCESS;
}
