#pragma once

#include <iostream>
#include <vector>
#include <memory>
#include <functional>
#include <tuple>
#include <GL/glew.h>

#include <glimac/glm.hpp>
#include <glimac/common.hpp>
#include <glimac/Cube.hpp>
#include <glimac/Functions.hpp>
#include <glimac/HeightMap.hpp>

using namespace glimac;
namespace masterCraft {

	/*** Décris l'objets 3d***/
	/* Rtn : Lst de tuple <Vao, vertexcount, fctransformation>
		Vao à en mémoire la texture donc le tuple permet de crée un bloque
		fctransformation : Rtn glmMat4 position, prend en argument : floatt speed, float time
		note : la valeur de retour bindTexture est vaoVboLst
	  		   la valeur de retour getVertexCount est vertexCount
	  		   Lien entre les position de vaoVboLst et vertexCount
	*/
	std::vector<std::tuple<GLuint, int, std::function<glm::mat4(double,double)>>> createTntBird(const std::vector<GLuint> &vaoVboLst,const std::vector<int> &vertexCount);
	/*Bind les textures dans des vbo et vao, renvoie les texture bind dans une liste*/
	std::vector<GLuint> bindTextureTntBird(int VERTEX_ATTR_POSITION, int VERTEX_ATTR_NORMAL, int VERTEX_ATTR_TEXCOORDS, int VERTEX_ATTR_TEXTURE);
	/*Renvoie VertexCount d'un object*/
	std::vector<int> getVertexCountTntBird();

	// Slime
	std::vector<std::tuple<GLuint, int, std::function<glm::mat4(double,double)>>> createCitrouSlime(const std::vector<GLuint> &vaoVboLst,const std::vector<int> &vertexCount);
	std::vector<GLuint> bindTextureCitrouSlime(int VERTEX_ATTR_POSITION, int VERTEX_ATTR_NORMAL, int VERTEX_ATTR_TEXCOORDS, int VERTEX_ATTR_TEXTURE);
	std::vector<int> getVertexCountCitrouSlime();

	// Cactus
	std::vector<std::tuple<GLuint, int, std::function<glm::mat4(double,double)>>> createCactus(const std::vector<GLuint> &vaoVboLst,const std::vector<int> &vertexCount);
	std::vector<GLuint> bindTextureCactus(int VERTEX_ATTR_POSITION, int VERTEX_ATTR_NORMAL, int VERTEX_ATTR_TEXCOORDS, int VERTEX_ATTR_TEXTURE);
	std::vector<int> getVertexCountCactus();

	// cherry tree
	std::vector<std::tuple<GLuint, int, std::function<glm::mat4(double,double)>>> createApple(const std::vector<GLuint> &vaoVboLst,const std::vector<int> &vertexCount);
	std::vector<GLuint> bindTextureApple(int VERTEX_ATTR_POSITION, int VERTEX_ATTR_NORMAL, int VERTEX_ATTR_TEXCOORDS, int VERTEX_ATTR_TEXTURE);
	std::vector<int> getVertexCountApple();

	// sapin
	std::vector<std::tuple<GLuint, int, std::function<glm::mat4(double,double)>>> createSapin(const std::vector<GLuint> &vaoVboLst,const std::vector<int> &vertexCount);
	std::vector<GLuint> bindTextureSapin(int VERTEX_ATTR_POSITION, int VERTEX_ATTR_NORMAL, int VERTEX_ATTR_TEXCOORDS, int VERTEX_ATTR_TEXTURE);
	std::vector<int> getVertexCountSapin();	


	// map
	std::vector<std::tuple<GLuint, int, std::function<glm::mat4(double,double)>>> createMap(const std::vector<GLuint> &vaoVboLst,const std::vector<int> &vertexCount);
	std::vector<GLuint> bindTextureMap(int VERTEX_ATTR_POSITION, int VERTEX_ATTR_NORMAL, int VERTEX_ATTR_TEXCOORDS, int VERTEX_ATTR_TEXTURE);
	std::vector<int> getVertexCountMap();

}