#pragma once

#include <iostream>
#include <vector>
#include <memory>
#include <functional>
#include <GL/glew.h>

#include <glimac/Npc.hpp>
#include <glimac/DeepMap.hpp>

using namespace glimac;
namespace masterCraft {

	// Cactus
	std::shared_ptr<DeepMap> generateDeepCactus();

	// Apple
	std::shared_ptr	<DeepMap> generateDeepApple();

	// Sapin
	std::shared_ptr	<DeepMap> generateDeepSapin();

}