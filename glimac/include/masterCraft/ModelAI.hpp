#pragma once

#include <iostream>
#include <vector>
#include <memory>
#include <functional>
#include <GL/glew.h>

#include <glimac/Npc.hpp>
#include <glimac/DeepMap.hpp>

using namespace glimac;
namespace masterCraft {

	// Function for npc (AI)
    // Création d'une cible : Doit prendre en compte l'existance d'un bloc sur la cible
    glm::vec3 generateCiblePosBird(const DeepMap& deepMap, const glm::vec3 &positionNpc);
    // Fait le déplacement
    glm::mat4 generateDeplacementBird(glm::vec3 &positionNpc, glm::vec3 &ang,const glm::vec3 &cibleNpc,const  float speed,const float speedAng);
    // prévoie la prochaine case Vec3 d'entier
    glm::vec3 generateNextPosBird(const glm::vec3 &positionNpc,const glm::vec3 &cibleNpc, const float speed);
    // Algo d'évitement de block
    glm::vec3 generateDodgeAIBird(const DeepMap& deepMap, const glm::vec3 &positionNpc, const glm::vec3& cibleNpc);


    glm::vec3 generateCiblePosCitrouSlime(const DeepMap& deepMap, const glm::vec3 &positionNpc );
    glm::mat4 generateDeplacementCitrouSlime(glm::vec3 &positionNpc, glm::vec3 &ang,const glm::vec3 &cibleNpc,const  float speed,const float speedAng);
    glm::vec3 generateNextPosCitrouSlime(const glm::vec3 &positionNpc,const glm::vec3 &cibleNpc, const float speed);
    glm::vec3 generateDodgeAICitrouSlime(const DeepMap& deepMap, const glm::vec3 &positionNpc ,const glm::vec3& cibleNpc);
}