#pragma once

#include <iostream>
#include <vector>
#include <exception>
#include "glm.hpp"
#include <omp.h>
namespace glimac {
    class DeepMap{

    public:
      /*Tableau 1D de la map (x,z) */
      /*A la pos x,z : liste de n valeur [1, 2,3 ,4,5 ,7,8 ... n]
      1er valeur = profoncdeur min, derniere valeur = plafon max couple / de 2 valeurs = interval de bloque
      Expl :
      [10 20 30] -> sol à 10, bloque entre 20 et 30
      */
      DeepMap(int width, int height):width(width), height(height){
        for (auto i = 0; i< height ; i++){
          for (auto j = 0; j< width ; j++){
            map.push_back(std::vector<int>());
            map.back().push_back(0);
          }
        }
      }

      ~DeepMap(){}

      int getWidth()const {return width;}
      int getHeight()const {return height;}

      int getGround(int x, int z) const;
      int getNeerGround(int x, int y, int z) const;
      int getNeerCeiling(int x, int y, int z) const;
      int getCeiling(int x, int z) const;
      const std::vector<int> getDeep(int x, int z) const;
      const std::vector<int> getDeepFromDeepMap(int x, int z) const;

      /*Elimine les valeurs en trop*/
      void setGround(int x, int z, int y);
      void setCeiling(int x, int z, int y);
      void setDeep(int x, int z , int yBegin, int yEnd);

      bool isEmpty(int x, int y, int z) const ;
      bool isEmptyPlayer(int x, int y, int z) const ;
      DeepMap& addDeep(const DeepMap& dm, glm::mat4 mat = glm::mat4(1));
      void print() const {
        for (auto i = -height/2.; i< height/2. ; i++){
          for (auto j = -width/2.; j< width/2. ; j++){
            std::cout <<" " << map[index(j,i)][0];
          }
            std::cout << std::endl;
        }
      }
      void generateDeepFromImage(std::vector<int> mapImage, int width, int height);
    private:
      int index(int x, int z) const ;
      int width;
      int height;
      std::vector<std::vector<int>> map;
    };


}


/***** -Test deepMap function
              auto test = new DeepMap(1,1);
              test->setCeiling(0,0,10);
              test->setGround(0,0,8);
              test->setCeiling(0,0,0);
              test->setGround(0,0,-5);
              test->setCeiling(0,0,0);
              test->setCeiling(0,0,-6);
              test->setGround(0,0,-6);
              test->setGround(0,0,-4);
              test->setDeep(0,0,-8,5);
              test->setDeep(0,0,-3,6);
              test->setDeep(0,0,8,9);
              test->setCeiling(0,0,20);
              test->setGround(0,0,-6);
              int veritas = 0;
              veritas += test->isEmpty(0,-8,0); // False
              veritas += test->isEmpty(0, 20,0); // False 
              veritas += test->isEmpty(0, 0,0); // False
              veritas += test->isEmpty(0, 7,0); // True 
              veritas += test->isEmpty(0, 10,0); // True 
              veritas += test->isEmpty(0, 15,0); // True 
              std::cout<< "AAA : " << test->getGround(0,0) << std::endl; // -6
              std::cout<< "BBB : " << test->getDeep(0,0).size()<<std::endl; //6
              std::cout<< "CCC : " << test->getCeiling(0,0) << std::endl;// -2
              std::cout  << "DDDD : ";  
              for (auto i : test->getDeep(0,0)){
                  std::cout  << i << " ";
              }
              std::cout<< std::endl;
              std::cout<< "EEE : " << veritas << std::endl; // 3

/*****/