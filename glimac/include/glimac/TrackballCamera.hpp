class TrackballCamera {
	private:
		float m_fDistance;
		float m_fAngleX;
		float m_fAngleY;
	public:
		TrackballCamera() {
			m_fDistance = -5.f;
			m_fAngleX = 0.f;
			m_fAngleY = 0.f;
		}
		void moveFront(float delta) {
			m_fDistance += delta;
		}
		void rotateLeft(float degrees) {
			m_fAngleX += degrees;
		}
		void rotateUp(float degrees) {
			m_fAngleY += degrees;
		}
		glm::mat4 getViewMatrix() const {
			glm::mat4 MVMatrix =  glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, m_fDistance));;
			MVMatrix = glm::rotate(MVMatrix, m_fAngleX, glm::vec3(0, 1, 0));
			MVMatrix = glm::rotate(MVMatrix, m_fAngleY, glm::vec3(1, 0, 0));
			return MVMatrix;
		}
	
};