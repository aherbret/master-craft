#pragma once

#include <iostream>
#include <vector>
#include <GL/glew.h>
#include "glm.hpp"
#include <map>
#include <memory>

namespace glimac {
class LightSource {
	public:
		LightSource(
			glm::vec3 LightDir_vs = glm::vec3(0.0,0.0,0.0),
			glm::vec3 LightIntensity = glm::vec3(0.0,0.0,0.0),
			glm::vec3 AmbientLight = glm::vec3(0.0,0.0,0.0)):LightDir_vs(LightDir_vs),LightIntensity(LightIntensity),AmbientLight(AmbientLight){};
		void uSend(std::shared_ptr<std::map<std::string, GLint>> uMap,const glm::vec3 &PosCam,const glm::vec3 &Center);
		void uSendAmbientLight(std::shared_ptr<std::map<std::string, GLint>> uMap);
		void setLightDir_vs(glm::vec3 nLightDir_vs){LightDir_vs = nLightDir_vs;}
		void setLightIntensity(glm::vec3 nLightIntensity){LightIntensity = nLightIntensity;}
		void setAmbientLight(glm::vec3 nAmbientLight){AmbientLight = nAmbientLight;}
	private:
		glm::vec3 LightDir_vs;
		glm::vec3 LightIntensity;
		glm::vec3 AmbientLight;
};

class LightBlock {
	public:
		LightBlock(glm::vec3 Kd = glm::vec3(0.0,0.0,0.0), glm::vec3 Ks = glm::vec3(0.0,0.0,0.0), float Shininess = 0):Kd(Kd),Ks(Ks),Shininess(Shininess){};
		void uSend(std::shared_ptr<std::map<std::string, GLint>> uMap);
	private:
    	glm::vec3 Kd;
    	glm::vec3 Ks;
      	float Shininess;
};

class LightState{
	public:
		// 0=false 1=true 2=ambuant light only
		LightState(float ApplyLight = 0.0f, 
			GLuint TextureArray = 0):TextureArray(TextureArray),ApplyLight(ApplyLight){};
		void uSend(std::shared_ptr<std::map<std::string, GLint>> uMap);
	private:
		GLuint TextureArray;
		float ApplyLight;
};

}

