#pragma once

#include <iostream>
#include <vector>
#include <GL/glew.h>
#include "glm.hpp"
#include "common.hpp"
#include "ElementForm.hpp"
#include "DeepMap.hpp"
#include "Cube.hpp"
#include <map>
#include <memory>
#include <functional>
#include <tuple>
#include <omp.h>
#include "Functions.hpp"

namespace glimac {

int findFirstElmVector(std::vector<glm::vec3> typeCubeColor, glm::vec3 Color);

int indexPos(int x, int z,int width,int height);

class Chunck{
    public:
    // Coordonné de la map
    static std::shared_ptr<ElementForm> generateMapElementForm(const DeepMap &deepMap, const std::vector<glm::vec3> &hColorMap, const std::vector<glm::vec3> &typeCubeColor, const std::vector<Cube> &typeCube, int xPos, int zPos,int sizeChunckX, int sizeChunckZ,
        GLuint texture,
        LightBlock *lightMaterial,
        int VERTEX_ATTR_POSITION, 
        int VERTEX_ATTR_NORMAL, 
        int VERTEX_ATTR_TEXCOORDS, 
        int VERTEX_ATTR_TEXTURE
        );
};

}