#pragma once

#include <glimac/SDLWindowManager.hpp>
#include <GL/glew.h>
#include <glimac/common.hpp>
#include <glimac/Image.hpp>
#include <iostream>

using namespace glimac;

glm::vec4* ImageToMapAtlass(unsigned int width, unsigned int height,unsigned int blockWidth,unsigned int blockHeight ,glm::vec4 *pixels);
void bindMapTexture(Image* pPicture, GLuint* arrayTexture, GLsizei width, GLsizei height, GLsizei layerCount );
void bindMapTexture(Image* pPicture, GLuint* arrayTexture, GLsizei width, GLsizei height);
