#pragma once

#include <glimac/SDLWindowManager.hpp>
#include <GL/glew.h>
#include <glimac/common.hpp>
#include <glimac/Image.hpp>
#include <iostream>
#include "glimac/Cube.hpp"

using namespace glimac;

//std::vector<int> loadHeightMap();
std::vector<int> loadHeightMap(std::unique_ptr<Image> heightMapImg);
Cube cubeType(int colorx, int colory, int colorz);
//std::vector<Cube> loadColoredHeightMap(std::unique_ptr<Image> coloredHeightMapImg);
std::vector<int> initHeightMap();
std::vector<glm::vec3> loadColoredMap(std::unique_ptr<Image> coloredHeightMapImg);