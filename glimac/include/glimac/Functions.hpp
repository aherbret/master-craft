#pragma once

#include <iostream>
#include <vector>
#include <map>
#include <memory>
#include <functional>
#include <tuple>
#include <GL/glew.h>

#include "Program.hpp"
#include "FilePath.hpp"
#include "Image.hpp"
#include "glm.hpp"
#include "common.hpp"
#include "TextureMap.hpp"


namespace glimac {

    //fusionnne les informations du ShapeVertex pour les mettre dans data.
    // Permet d'envoyer les information au vao en plus grande quantité
    void fuseData(std::vector<float>& data,const ShapeVertex* ptr, int VertexCount, glm::mat4 transfo);
	void fuseBigData(float *data, int posData , const ShapeVertex* ptr, int VertexCount, glm::mat4 transfo);
	// init all UniformeVar in a map	
	std::shared_ptr<std::map<std::string, GLint>> initUniformeVar (Program &program);
	// init all texture in a map
	std::map<std::string, GLuint> initUniformTex(GLuint *&texture, GLuint *&arrayTexture, const std::string &path);
	// free texture allocate by initUniformTex
	void freeTex(GLuint *&texture, GLuint *&arrayTexture);

}