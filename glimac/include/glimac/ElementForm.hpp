#pragma once

#include <iostream>
#include <vector>
#include <GL/glew.h>
#include "glm.hpp"
#include "common.hpp"
#include "Cube.hpp"
#include "LightBlock.hpp"
#include <map>
#include <memory>
#include <functional>
#include <tuple>

namespace glimac {

class ElementForm{
    public:
        ElementForm(
            float speed,
            GLuint texture,
            LightBlock *lightMaterial,
            std::vector<GLuint> vaoVboLst,
            std::vector<std::tuple<GLuint, int, std::function<glm::mat4(double,double)>>> tupleList
        ):vaoVboLst(vaoVboLst), tupleList(tupleList),speed(speed),texture(texture),lightMaterial(lightMaterial){};

        ~ElementForm(){
            //std::cout << "UnBind" << std::endl; 
            uBindBuffers();
        }

        void uDraw(float time, std::shared_ptr<std::map<std::string, GLint>> uMap,
            const glm::vec3 &posCam,
            const glm::mat4 &ProjMatrix, 
            const glm::mat4 &MVMatrix, 
            const glm::mat4 &NormalMatrix, 
            const glm::mat4 &deplacementGeneral,
            LightSource &lightSrc);

        static std::shared_ptr<ElementForm> CreateElementForm(
            float speed,
            GLuint texture,
            LightBlock *lightMaterial,
            std::function<std::vector<GLuint>(int,int,int,int)> bind,
            std::function<std::vector<std::tuple<GLuint, int, std::function<glm::mat4(double,double)>>>(const std::vector<GLuint>,const std::vector<int>)>draw,
            std::function<std::vector<int>()>getVertexCount,
            int VERTEX_ATTR_POSITION, 
            int VERTEX_ATTR_NORMAL, 
            int VERTEX_ATTR_TEXCOORDS, 
            int VERTEX_ATTR_TEXTURE);

        void setSpeed(float speed){speed = speed;}
        
    private:
        void uBindBuffers();
        std::vector<GLuint> vaoVboLst;
        std::vector<std::tuple<GLuint, int, std::function<glm::mat4(double,double)>>> tupleList;
        float speed;
        GLuint texture;
        LightBlock *lightMaterial;
};

}

