#pragma once
#include "glimac/glm.hpp"

class FreeFlyCamera{

public:
	FreeFlyCamera();
	void computeDirectionVectorsFly();
	void moveLeftFly(float t);
	void moveFrontFly(float t);
	void moveUpFly(float t);
	void rotateLeft(float degrees);
	void rotateUp(float degrees);

	void computeDirectionVectors();
	void moveLeft(float t);
	void moveFront(float t);
	void moveUp(float t);
	void setXYZ(glm::vec3 pos);

	glm::mat4 getViewMatrix() const;

	float getPhi(){return m_fPhi;}
	float getTheta(){return m_fTheta;}
	glm::vec3 getPos(){return m_Position;}
	
private:
	glm::vec3 m_Position;
	float m_fPhi;
	float m_fTheta;

	glm::vec3 m_FrontVector;
	glm::vec3 m_LeftVector;
	glm::vec3 m_UpVector;

};