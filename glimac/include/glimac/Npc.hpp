#pragma once

#include <iostream>
#include <vector>
#include <GL/glew.h>
#include "glm.hpp"
#include "common.hpp"
#include "ElementForm.hpp"
#include "DeepMap.hpp"
#include <map>
#include <memory>
#include <functional>
#include <tuple>

namespace glimac {

    class subNpc{

    public:
        subNpc(float speed = 0,
            float speedAng = 0,
            glm::vec3 positionNpc = glm::vec3(0,0,0),
            glm::vec3 cibleNpc = glm::vec3(0,0,0),
            glm::vec3 cibleNpcIntermediaire = glm::vec3(0,0,0),
            glm::vec3 angleNpc = glm::vec3(0,0,0),
            glm::mat4 deplacement = glm::mat4(1)
            ):speed(speed),speedAng(speedAng),positionNpc(positionNpc),cibleNpc(cibleNpc),angleNpc(angleNpc),cibleNpcIntermediaire(cibleNpcIntermediaire),deplacement(deplacement){}

        void setSpeed(float speed, float speedAng);
        void reset();

    private:
            float speed;
            float speedAng;
            glm::vec3 positionNpc;
            glm::vec3 cibleNpc;
            glm::vec3 cibleNpcIntermediaire;
            glm::vec3 angleNpc;
            glm::mat4 deplacement;
            friend class Npc; 
    };
    /*
const DeepMap& deepMap
const glm::vec3 &positionNpc*/

    class Npc{
        public :
            Npc(std::function<glm::vec3(const DeepMap&, const glm::vec3&)> generateCible,  
                std::function<glm::mat4(glm::vec3&, glm::vec3&,const  glm::vec3&,const  float,const float)> generateDeplacement,
                std::function<glm::vec3 (const glm::vec3&, const glm::vec3&,const  float)> generateNextPos,
                std::function<glm::vec3 (const DeepMap&, const glm::vec3& , const glm::vec3&)> generateDodgeAI
                ):generateCible(generateCible),generateDeplacement(generateDeplacement),generateNextPos(generateNextPos),generateDodgeAI(generateDodgeAI),hasAI(true),hasForm(false){};
            
            void setElementForm(float speed,
                GLuint texture,
                LightBlock *lightMaterial,
                std::function<std::vector<GLuint>(int,int,int,int)> bind,
                std::function<std::vector<std::tuple<GLuint, int, std::function<glm::mat4(double,double)>>>(const std::vector<GLuint>,const std::vector<int>)>draw,
                std::function<std::vector<int>()>getVertexCount,
                int VERTEX_ATTR_POSITION,
                int VERTEX_ATTR_NORMAL,
                int VERTEX_ATTR_TEXCOORDS,
                int VERTEX_ATTR_TEXTURE
                );
            
            void uDraw(float time,
                std::shared_ptr<std::map<std::string, GLint>> uMap,
                const glm::vec3 &posCam,
                const glm::mat4 &ProjMatrix,
                const glm::mat4 &MVMatrix, 
                const glm::mat4 &NormalMatrix, 
                LightSource &lightSrc,
                subNpc &npcPos
            );
            
            void setSpeedAnim(float speedAnim) {form->setSpeed(speedAnim);}
            bool isReady() {return (hasAI && hasForm);}

            /** Genere une nouvelle cile si possible **/
            /** Si collision génère une cible intermediaire **/
            bool updateCible(subNpc &npcPos, const DeepMap& deepMap);



        private :
            glm::vec3 nextPosGenerate(const subNpc &npcPos){
                return generateNextPos(npcPos.positionNpc,npcPos.cibleNpcIntermediaire, npcPos.speed);
            }
            glm::mat4 deplacementGeneral(subNpc &npcPos){
                return generateDeplacement(npcPos.positionNpc,npcPos.angleNpc,npcPos.cibleNpcIntermediaire , npcPos.speed, npcPos.speedAng);
            }

            bool hasAI;
            bool hasForm;

            std::shared_ptr<ElementForm> form;
            std::function<glm::mat4  (glm::vec3&, glm::vec3&, const glm::vec3&,const  float,const float)> generateDeplacement;
            std::function<glm::vec3 (const glm::vec3&, const glm::vec3&,const  float)> generateNextPos;
            std::function<glm::vec3(const DeepMap&, const glm::vec3&)> generateCible;
            std::function<glm::vec3 (const DeepMap&, const glm::vec3&, const glm::vec3&)> generateDodgeAI;
    };
    
    
}


