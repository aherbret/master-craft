#include "masterCraft/ModelDeep.hpp"

namespace masterCraft {

	// Cactus
	std::shared_ptr<DeepMap> generateDeepCactus(){
		auto map = std::shared_ptr<DeepMap>(new DeepMap(3,3));
		map->setDeep(0,0,0,1);
		return map;
	}

	// Apple
	std::shared_ptr	<DeepMap> generateDeepApple(){
		auto map = std::shared_ptr<DeepMap>(new DeepMap(5,5));
		map->setDeep(0,0,0,4);/*
		map->setDeep(0,1,0,4);
		map->setDeep(1,0,0,4);
		map->setDeep(0,-1,0,4);
		map->setDeep(-1,0,0,4);*/
		for (auto i = -1; i < 2 ; i++){
			for (auto j = -1 ; j< 2 ; j++){
				if (i !=0 || j !=0){
					map->setDeep(i,j,1,3);					
				}
			}
		}
		return map;	
	}

	// Sapin
	std::shared_ptr	<DeepMap> generateDeepSapin(){
		auto map = std::shared_ptr<DeepMap>(new DeepMap(7,7));
		map->setDeep(0,0,0,5);/*
		map->setDeep(0,1,0,5);
		map->setDeep(1,0,0,5);
		map->setDeep(0,-1,0,5);
		map->setDeep(-1,0,0,5);*/
		for (auto i = -1; i < 2 ; i++){
			for (auto j = -1 ; j< 2 ; j++){
				if (i !=0 || j !=0){
					map->setDeep(i,j,2,4);					
				}
			} 
		}
	    for (auto x = -1; x< 2; x++){
				map->setDeep(x,2,2,3);
				map->setDeep(x,-2,2,3);
				map->setDeep(2,x,2,3);
				map->setDeep(-2,x,2,3);

	    }

		return map;	
	}
}