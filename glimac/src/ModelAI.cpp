#include "masterCraft/ModelAI.hpp"

namespace masterCraft {
    

int round(double value) {
     return floor(value + 0.5);
}
/************************************************************* BirdTnt *************************************************************/
glm::mat4 generateDeplacementBird(glm::vec3 &positionNpc, glm::vec3 &ang,const glm::vec3 &cibleNpc,const float speed,const float speedAng){
   auto dist = (cibleNpc-positionNpc);

   float angX = ang.x;
   float angY = ang.y;
   float angZ = ang.z;
    
   float rAxeY = 0;
   float rAxeX = 0;
   //float rAxeX = atan(dist.z/dist.y);
    if (dist.z == 0 && dist.x != 0){
      if (dist.x > 0) rAxeY = glm::radians(-90.0f);
      else rAxeY = glm::radians(90.0f);
    }
    else if (dist.x == 0 && dist.z != 0){
      if (dist.z > 0) rAxeY = glm::radians(180.0f);
      else rAxeY = 0;
    }
    else if (dist.z < 0 && dist.x > 0){
      rAxeY = glm::radians(-45.0f);
    }
    else if (dist.z > 0 && dist.x < 0){
      rAxeY = glm::radians(135.0f);
    }
    else if (dist.z > 0 && dist.x > 0){
      rAxeY = glm::radians(-135.0f);
    }
    else if (dist.z < 0 && dist.x < 0){
      rAxeY = glm::radians(45.0f);
    }

    if (dist.y == 0){
      rAxeX = 0;
    }
    else if (dist.x == 0 && dist.z == 0){
      rAxeX = 0;
    }
    else if (dist.y > 0){
      rAxeX = glm::radians(45.0f);
    }
    else if (dist.y < 0){
      rAxeX = glm::radians(-45.0f);
    }

   /* Move */
   if (dist.x > speed){
      positionNpc += glm::vec3(speed,0,0);
   }
   else if (dist.x < -speed){
      positionNpc -= glm::vec3(speed,0,0);
   }
   else {
    positionNpc = glm::vec3(cibleNpc.x,positionNpc.y,positionNpc.z);
   }
   if (dist.y > speed){
      positionNpc += glm::vec3(0,speed,0);
   }
   else if (dist.y < -speed){
      positionNpc -= glm::vec3(0,speed,0);
   }
   else {
    positionNpc = glm::vec3(positionNpc.x,cibleNpc.y,positionNpc.z);
   }
   if (dist.z > speed){
      positionNpc += glm::vec3(0,0,speed);
   }
   else if (dist.z < -speed){
      positionNpc -= glm::vec3(0,0,speed);
   }
   else {
    positionNpc = glm::vec3(positionNpc.x,positionNpc.y,cibleNpc.z);
   }
       /*
       float angX = 0;
      float angY = 0;*/

   if (angX < rAxeX+speedAng){
    angX += speedAng;
   }
    if (angX > rAxeX-speedAng){
    angX -= speedAng;
   }
   if (angY < rAxeY+speedAng){
    angY += speedAng;
   }
   if (angY > rAxeY-speedAng){
    angY -= speedAng;
   }
   //float rAxeZ = atan(dist.y/dist.x);

    float rAxeZ = 0;
   //std::cout << "Ang " << glm::degrees(rAxeX) << '\n';
   //std::cout << "Ang " << glm::degrees(rAxeY) << '\n';

    auto deplacementGeneral = glm::translate(glm::mat4(1) ,positionNpc);
    deplacementGeneral = glm::rotate( deplacementGeneral ,(angY),glm::vec3(0, 1, 0));
    deplacementGeneral = glm::rotate( deplacementGeneral ,(angX),glm::vec3(1, 0, 0));

    ang.x = angX;
    ang.y = angY;
    ang.z = angZ;
    
    return deplacementGeneral;
}


glm::vec3 generateNextPosBird(const glm::vec3 &positionNpc,const glm::vec3 &cibleNpc,const float speed){
   glm::vec3 dist = (cibleNpc-positionNpc);
   glm::vec3 pos = glm::vec3(positionNpc.x,positionNpc.y,positionNpc.z);
   int speedCase = std::ceil(speed);
   if (dist.x > speed){
      pos += glm::vec3(speedCase,0,0);
   }
   else if (dist.x < -speed){
      pos -= glm::vec3(speedCase,0,0);
   }
   else {
    pos = glm::vec3(cibleNpc.x,pos.y,pos.z);
   }

   if (dist.y > speed){
      pos += glm::vec3(0,speedCase,0);
   }
   else if (dist.y < -speed){
      pos -= glm::vec3(0,speedCase,0);
   }
   else {
    pos = glm::vec3(pos.x,cibleNpc.y,pos.z);
   }

   if (dist.z > speed){
      pos += glm::vec3(0,0,speedCase);
   }
   else if (dist.z < -speed){
      pos -= glm::vec3(0,0,speedCase);
   }
   else {
    pos = glm::vec3(pos.x,pos.y,cibleNpc.z);
   }

   pos = glm::vec3(round(pos.x),round(pos.y),round(pos.z));
   return pos;
}
/*** Position AI ***/
glm::vec3 generateCiblePosBird(const DeepMap& deepMap, const glm::vec3 &positionNpc){

  int x = (25-(std::rand()%50));
  int z = (25-(std::rand()%50));
  int y = (25-(std::rand()%50))*2 + positionNpc.y;
  while (((positionNpc.x+x*2) <= -deepMap.getWidth() || (positionNpc.x+x*2) >= deepMap.getWidth()) 
    ||((positionNpc.z+z*2) <= -deepMap.getWidth() ||( positionNpc.z+z*2) >= deepMap.getWidth())
    || !deepMap.isEmpty(positionNpc.x+x*2,y,positionNpc.z+z*2)){
    x = (25-(std::rand()%50));
    z = (25-(std::rand()%50));
    y = (25-(std::rand()%50))*2 + positionNpc.y;
    try{
        if (!deepMap.isEmpty(positionNpc.x+x*2,y,positionNpc.z+z*2)){
            //std::cout << "Warning Colision pos " << glm::vec3(positionNpc.x+x*2,positionNpc.y+y,positionNpc.z+z*2) << std::endl;
            auto lstProfondeur = deepMap.getDeep(positionNpc.x+x*2,positionNpc.z+z*2);
            if (lstProfondeur.size() != 0){
               y = (lstProfondeur.back())+4;      
            }
            else{
              y = deepMap.getGround(positionNpc.x+x*2,positionNpc.z+z*2)+4; 
            }
      }
    }
    catch(...){
      //std::cout << "Warning npc out of map Bird" << std::endl;
    }
  }
  if (y > 128){
     y = 128;
  }
  else  if (y < -128){
     y = -128;
  }
//  std::cout << glm::vec3(x,y,z) << " " << positionNpc << std::endl;
  return glm::vec3(positionNpc.x+x*2,y,positionNpc.z+z*2);

}

glm::vec3 generateDodgeAIBird(const DeepMap& deepMap, const glm::vec3 &positionNpc,const glm::vec3& cibleNpc){
    auto tmp = cibleNpc-positionNpc;
    if (tmp.x > 0){
      tmp.x = -(std::rand()%10);
    }
    else {
      tmp.x = (std::rand()%10);  
    }
    if (tmp.z > 0){
      tmp.z = -(std::rand()%10);
    }
    else {
      tmp.z = (std::rand()%10);  
    }
    
    if (tmp.y > 0){
      tmp.y = (std::rand()%10);
       
    }
    else if (tmp.y < 0 ) {
      tmp.y = -(std::rand()%10);  

    }
    else{
      tmp.y = 0;
    }
    
    try {
        if (!deepMap.isEmpty(positionNpc.x,positionNpc.y-20 , positionNpc.z)){
          tmp = cibleNpc-positionNpc;
          tmp.y = positionNpc.y +20;
        }
    }
    catch(...){
        throw std::length_error("NPC Bird is out of the map : X or Z error"); 
    }

    auto pos = glm::vec3(positionNpc.x + tmp.x, tmp.y , positionNpc.z + tmp.z);
    return pos;
}


/************************************************************* citrouSlime *************************************************************/

glm::mat4 generateDeplacementCitrouSlime(glm::vec3 &positionNpc, glm::vec3 &ang,const glm::vec3 &cibleNpc,const float speed,const float speedAng){
   auto dist = (cibleNpc-positionNpc);

   float angX = ang.x;
   float angY = ang.y;
   float angZ = ang.z;
    
   float rAxeY = 0;
   float rAxeX = 0;

    if (dist.y == 0){
      rAxeX = 0;
    }
    else if (dist.x == 0 && dist.z == 0){
      rAxeX = 0;
    }
    else if (dist.y > 0){
      rAxeX = glm::radians(45.0f);
    }
    else if (dist.y < 0){
      rAxeX = glm::radians(-45.0f);
    }

   /* Move */
   if (dist.x > speed){
      positionNpc += glm::vec3(speed,0,0);
   }
   else if (dist.x < -speed){
      positionNpc -= glm::vec3(speed,0,0);
   }
   else {
    positionNpc = glm::vec3(cibleNpc.x,positionNpc.y,positionNpc.z);
   }
   if (dist.y > speed){
      positionNpc += glm::vec3(0,speed,0);
   }
   else if (dist.y < -speed){
      positionNpc -= glm::vec3(0,speed,0);
   }
   else {
    positionNpc = glm::vec3(positionNpc.x,cibleNpc.y,positionNpc.z);
   }
   if (dist.z > speed){
      positionNpc += glm::vec3(0,0,speed);
   }
   else if (dist.z < -speed){
      positionNpc -= glm::vec3(0,0,speed);
   }
   else {
    positionNpc = glm::vec3(positionNpc.x,positionNpc.y,cibleNpc.z);
   }
       /*
       float angX = 0;
      float angY = 0;*/

   if (angX < rAxeX+speedAng){
    angX += speedAng;
   }
    if (angX > rAxeX-speedAng){
    angX -= speedAng;
   }
   if (angY < rAxeY+speedAng){
    angY += speedAng;
   }
   if (angY > rAxeY-speedAng){
    angY -= speedAng;
   }
   //float rAxeZ = atan(dist.y/dist.x);

    float rAxeZ = 0;
   //std::cout << "Ang " << glm::degrees(rAxeX) << '\n';
   //std::cout << "Ang " << glm::degrees(rAxeY) << '\n';

    auto deplacementGeneral = glm::translate(glm::mat4(1) ,positionNpc);
    deplacementGeneral = glm::rotate( deplacementGeneral ,(angY),glm::vec3(0, 1, 0));
    deplacementGeneral = glm::rotate( deplacementGeneral ,(angX),glm::vec3(1, 0, 0));

    ang.x = angX;
    ang.y = angY;
    ang.z = angZ;
    
    return deplacementGeneral;
}


glm::vec3 generateNextPosCitrouSlime(const glm::vec3 &positionNpc,const glm::vec3 &cibleNpc,const float speed){
   auto dist = (cibleNpc-positionNpc);
   glm::vec3 pos = glm::vec3(positionNpc.x,positionNpc.y,positionNpc.z);
   int speedCase = std::ceil(speed);
   if (dist.x > speed){
      pos += glm::vec3(speedCase,0,0);
   }
   else if (dist.x < -speed){
      pos -= glm::vec3(speedCase,0,0);
   }
   else {
    pos = glm::vec3(cibleNpc.x,pos.y,pos.z);
   }
   if (dist.z > speed){
      pos += glm::vec3(0,0,speedCase);
   }
   else if (dist.z < -speed){
      pos -= glm::vec3(0,0,speedCase);
   }
   else {
    pos = glm::vec3(pos.x,pos.y,cibleNpc.z);
   }
   pos = glm::vec3(round(pos.x),round(pos.y),round(pos.z));
   return pos;
}
/*** Position AI ***/
glm::vec3 generateCiblePosCitrouSlime(const DeepMap& deepMap, const glm::vec3 &positionNpc){
  int x = 0;
  int z = 0;
  int y = 0;
  while (((positionNpc.x+x*2) <= -deepMap.getWidth() || (positionNpc.x+x*2) >= deepMap.getWidth()) 
    ||((positionNpc.z+z*2) <= -deepMap.getWidth() ||( positionNpc.z+z*2) >= deepMap.getWidth())
    || (y != positionNpc.y)){
    x = (10-(std::rand()%20));
    z = (10-(std::rand()%20));

    try{
      y = deepMap.getGround(positionNpc.x+x*2, positionNpc.z+z*2)+2;
    }
    catch(...){
      y = -600;
      //std::cout << "Warning out npc of map Slime" << std::endl;
    }
  }
  //std::cout << glm::vec3(x,y,z) << " " << positionNpc << std::endl;
  return glm::vec3(positionNpc.x+x*2,y,positionNpc.z+z*2);
}

glm::vec3 generateDodgeAICitrouSlime(const DeepMap& deepMap, const glm::vec3 &positionNpc,const glm::vec3& cibleNpc){
    auto tmp = cibleNpc-positionNpc;
 
  int x = 0;
  int z = 0;
  int y = 0;
  while (((positionNpc.x+x*2) <= -deepMap.getWidth() || (positionNpc.x+x*2) >= deepMap.getWidth()) 
    ||((positionNpc.z+z*2) <= -deepMap.getWidth() ||( positionNpc.z+z*2) >= deepMap.getWidth())
    || (y != positionNpc.y)){


    if (x > 0){
      x = -(std::rand()%10);
    }
    else {
      x = (std::rand()%10);  
    }
    if (z > 0){
      z = -(std::rand()%10);
    }
    else {
      z = (std::rand()%10);  
    }

    try{
      y = deepMap.getGround(positionNpc.x+x*2, positionNpc.z+z*2)+2;
    }
    catch(...){
      y = -600;
      //std::cout << "Warning out npc of map Slime" << std::endl;
    }
  }
  //std::cout << glm::vec3(x,y,z) << " " << positionNpc << std::endl;
  return glm::vec3(positionNpc.x+x*2,y,positionNpc.z+z*2);
}

}
