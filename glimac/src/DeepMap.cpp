#include "glimac/DeepMap.hpp"

namespace glimac {

int DeepMap::getGround(int x, int z)  const{
	int posX = (int)x/2.;
	int posZ = (int)z/2.;
    return 2*map[index(posX,posZ)][0];
}

int DeepMap::getNeerGround(int x, int y, int z) const{
	int posX = (int)x/2.;
	int posZ = (int)z/2.;
	int posY = (int)y/2.;
    auto lstDeep = map[index(posX,posZ)];
    if (lstDeep.size() == 1){
    	return 2*lstDeep[0];
    }
	if (lstDeep.size() > 1){
    	std::cout << "Get Deep " << lstDeep.size() << std::endl;
    	std::cout << lstDeep[1]  << " " << lstDeep[2] << std::endl;
    }
    if (lstDeep.size()%2 == 0){
		for (auto i = lstDeep.size()-2; i> 0; i-=2){
			if (lstDeep[i] <= posY ){
				return 2*lstDeep[i];
			}
		}
    }
    else {
		for (auto i = lstDeep.size()-1; i> 0; i-=2){
	    	std::cout << "Value Try" << lstDeep[i] << " i "<< i << " " << posY << std::endl;
			if (lstDeep[i] <= posY ){
		    	std::cout << "Rtn" << std::endl;

				return 2*lstDeep[i];
			}
		}
    }

	return 2*lstDeep[0];
}

int DeepMap::getNeerCeiling(int x, int y, int z) const{
	int posX = (int)x/2.;
	int posZ = (int)z/2.;
	int posY = (int)y/2.;
    auto lstDeep = map[index(posX,posZ)];
    if (lstDeep.size()%2 == 0){
		for (auto i = 1; i< map[index(posX,posZ)].size()-1; i+=2){
			if (map[index(posX,posZ)][i] >= posY ){
				return map[index(posX,posZ)][i];
			}
		}
    }
    else {
		for (auto i = 1; i< map[index(posX,posZ)].size(); i+=2){
			if (map[index(posX,posZ)][i] >= posY ){
				return map[index(posX,posZ)][i];
			}
		}
    }

	return 128.;
}

int DeepMap::getCeiling(int x, int z)  const{
	int posX = (int)x/2.;
	int posZ = (int)z/2.;
	if (map[index(x,z)].size()%2 == 1){
		return 128.;
	}
    return 2*(map[index(posX,posZ)]).back();
}

const std::vector<int> DeepMap::getDeep(int x, int z) const{
	int posX = (int)x/2.;
	int posZ = (int)z/2.;
	std::vector<int> vec;
	if (map[index(posX,posZ)].size() <= 2){
		return std::vector<int>();
	}
	else if (map[index(posX,posZ)].size()%2 == 0){
		for (auto i = 1; i<map[index(posX,posZ)].size()-1; i++){
			vec.push_back(map[index(posX,posZ)][i]*2);
		}
		return vec;
	}

	for (auto i = 1; i<map[index(posX,posZ)].size(); i++){
		vec.push_back(map[index(posX,posZ)][i]*2);
	}
	return vec;
}

const std::vector<int> DeepMap::getDeepFromDeepMap(int x, int z) const{
	if (map[index(x,z)].size() <= 2){
		return std::vector<int>();
	}
	else if (map[index(x,z)].size()%2 == 0){
		return std::vector<int>(map[index(x,z)].begin()+1, map[index(x,z)].end()-1);
	}
	return std::vector<int>(map[index(x,z)].begin()+1, map[index(x,z)].end());
}

/*Version simple*/
void DeepMap::setGround(int x, int z, int y){
	if (map[index(x,z)].size()%2 == 0 && map[index(x,z)][map[index(x,z)].size()-1] < y){
		map[index(x,z)].pop_back();		
	}
	map[index(x,z)][0] = y;
	return;
}
/*Version simple*/
void DeepMap::setCeiling(int x, int z, int y){
	if (map[index(x,z)][0] >= y){
		return;
	}
	if (map[index(x,z)].size()%2 == 1){
		map[index(x,z)].push_back(y);
		return;
	}
	else {
		map[index(x,z)].back() = y;
	}

}

DeepMap& DeepMap::addDeep(const DeepMap& dm, glm::mat4 mat){
	for (auto h = 0; h< dm.height ; h++){
	  for (auto  l = 0; l< dm.width ;  l++){
	      int x =  l-(int)(dm.width/2.);
	      int z =  h-(int)(dm.height/2.);
	      auto vec = dm.getDeepFromDeepMap(x,z);
	      auto elm = glm::vec4(0,0,0,1);
	      for (auto i = 0; i< vec.size(); i+=2){
	          elm = glm::vec4(x,vec[i],z,1);
	          elm = mat * elm; // mv info 
	          auto elm2 = glm::vec4(x,vec[i+1],z,1);
	          elm2 = mat * elm2; // mv info 
	          this->setDeep(elm.x,elm.z,elm.y, elm2.y);  
	      }
	  }
	}
	return *this;
}
// Map // width 
void DeepMap::generateDeepFromImage(std::vector<int> mapImage, int width, int height){
          int i = 0;
          int nbThread = 100;
          omp_set_num_threads(nbThread);
          int sizeThread = (width)*(height)/nbThread;
          #pragma omp parallel
          {
            int num = omp_get_thread_num();
			for ( auto pos = sizeThread*num; pos < sizeThread*num+sizeThread; pos++ ){
              int x = (pos%(width))-(int)(width/2.);
              int y = (pos/(width))-(int)(height/2.);

              setGround(x,y,mapImage[index(x, y)]-128);
              //std::cout << "Set groun " << x << " " << y << " Y " << mapImage[index(y, x)]-128 << std::endl;
             }
          }
          for ( auto pos = sizeThread*nbThread; pos < (width)*(height); pos++ ){
              int x = (pos%(width))-(int)(width/2.);
              int y = (pos/(width))-(int)(height/2.);

              setGround(x,y,mapImage[index(x, y)]-128);
              //std::cout << "Set groun " << x << " " << y << " Y " << mapImage[pos]-128 << std::endl;
          }
      }

// yBegin > yEnd
void DeepMap::setDeep(int x, int z , int yBegin, int yEnd){
	int size = map[index(x,z)].size();
	if (size%2 == 0){
		size -= 1;
	}
	for (auto i = 1 ; i< size; i+=2){
		if (map[index(x,z)][i] > yBegin){
			if (map[index(x,z)].size() %2 == 0){
				map[index(x,z)].insert(map[index(x,z)].begin()+i, yBegin);
				map[index(x,z)].insert(map[index(x,z)].begin()+i, yEnd); 
				return;
			}
		}
	}
	if (map[index(x,z)].size()%2 == 1){
		map[index(x,z)].push_back(yBegin);
		map[index(x,z)].push_back(yEnd);	
		return;	
	}
	else {
		map[index(x,z)].insert(map[index(x,z)].end()-1, yBegin);
		map[index(x,z)].insert(map[index(x,z)].end()-1, yEnd); 	
		return;
	}

}

bool DeepMap::isEmptyPlayer(int x, int y, int z) const {
	int posX = (int)(x)/2.;
	int posZ = (int)(z)/2.;
	int posY = (int)y/2.;
	if (map[index(posX,posZ)][0] >= posY){
		return false;
	} 
	else if (map[index(posX,posZ)].size()%2 == 0 && map[index(posX,posZ)].back() <= posY){
		return false;
	} /*
	if ( map[index(posX,posZ)].size() > 1){
		std::cout << "Map idx size" << map[index(posX,posZ)].size() << " " << glm::vec3(posX,posY, posZ) <<std::endl;
		std::cout <<  map[index(posX,posZ)][0] << " " << map[index(posX,posZ)][1] <<std::endl;
		std::cout << "Pos "  <<  glm::vec3(x,y, z)  << std::endl;
	}*//*
	if ( map[index(posX+1,posZ)].size() > 1){
		std::cout << "Map idx size" << map[index(posX,posZ)].size() << " " << glm::vec3(posX,posY, posZ) <<std::endl;
		std::cout <<  map[index(posX,posZ)][0] << " " << map[index(posX,posZ)][1] <<std::endl;
	}*//*
	else {
		std::cout << "Pos "  <<  glm::vec3(x,y, z)  << std::endl;
	}*/
	for (auto i = 1; i< map[index(posX,posZ)].size()-1; i+=2){
		if (map[index(posX,posZ)][i] <= posY && map[index(posX,posZ)][i+1] >= posY){
			return false;
		}
	} 

	if (x%2 == -1){
		posX -= 1;
		for (auto i = 1; i< map[index(posX,posZ)].size()-1; i+=2){
			if (map[index(posX,posZ)][i] <= posY && map[index(posX,posZ)][i+1] >= posY){
				return false;
			}
		}
		posX += 1;
	}
	else if (x%2 == +1){
		posX += 1;
		for (auto i = 1; i< map[index(posX,posZ)].size()-1; i+=2){
			if (map[index(posX,posZ)][i] <= posY && map[index(posX,posZ)][i+1] >= posY){
				return false;
			}
		}
		posX -= 1;
	}

	if (z%2 == -1){
		posZ -= 1;
		for (auto i = 1; i< map[index(posX,posZ)].size()-1; i+=2){
			if (map[index(posX,posZ)][i] <= posY && map[index(posX,posZ)][i+1] >= posY){
				return false;
			}
		}
	}
	else if (z%2 == +1){
		posZ += 1;
		for (auto i = 1; i< map[index(posX,posZ)].size()-1; i+=2){
			if (map[index(posX,posZ)][i] <= posY && map[index(posX,posZ)][i+1] >= posY){
				return false;
			}
		}
	}

	if (x%2 == -1){
		posX -= 1;
		for (auto i = 1; i< map[index(posX,posZ)].size()-1; i+=2){
			if (map[index(posX,posZ)][i] <= posY && map[index(posX,posZ)][i+1] >= posY){
				return false;
			}
		}
	}
	else if (x%2 == +1){
		posX += 1;
		for (auto i = 1; i< map[index(posX,posZ)].size()-1; i+=2){
			if (map[index(posX,posZ)][i] <= posY && map[index(posX,posZ)][i+1] >= posY){
				return false;
			}
		}
	}

	return true;

}
bool DeepMap::isEmpty(int x, int y, int z) const {
	int posX = (int)x/2.;
	int posZ = (int)z/2.;
	int posY = (int)y/2.;
	if (map[index(posX,posZ)][0] >= posY){
		return false;
	} 
	
	else if (map[index(posX,posZ)].size()%2 == 0 && map[index(posX,posZ)].back() <= posY){
		return false;
	} 

	for (auto i = 1; i< map[index(posX,posZ)].size()-1; i+=2){
		if (map[index(posX,posZ)][i] <= posY && map[index(posX,posZ)][i+1] >= posY){
			return false;
		}
	} 
	return true;
}



int DeepMap::index(int x, int z) const {
    auto posX = x+(int)(width/2.);
    auto posZ = z+(int)(height/2.);
    if (posX < 0 || posX >= width){
      throw std::range_error("X outside of the map");
    }
    if (posZ < 0 || posZ >= height){
      throw std::range_error("Z outside of the map");
    }
	return posX+posZ*width;
}

}