#include "glimac/ElementForm.hpp"

namespace glimac {

std::shared_ptr<ElementForm> ElementForm::CreateElementForm(
			float speed,
            GLuint texture,
            LightBlock *lightMaterial,
            std::function<std::vector<GLuint>(int,int,int,int)> bind,
            std::function<std::vector<std::tuple<GLuint, int, std::function<glm::mat4(double,double)>>>(const std::vector<GLuint>,const std::vector<int>)>draw,
            std::function<std::vector<int>()>getVertexCount,
            int VERTEX_ATTR_POSITION, 
            int VERTEX_ATTR_NORMAL, 
            int VERTEX_ATTR_TEXCOORDS, 
            int VERTEX_ATTR_TEXTURE)
{ 
    std::vector<GLuint> vaoVboLst = bind( VERTEX_ATTR_POSITION, VERTEX_ATTR_NORMAL, VERTEX_ATTR_TEXCOORDS, VERTEX_ATTR_TEXTURE);
    std::vector<std::tuple<GLuint, int, std::function<glm::mat4(double,double)>>> tupleLst = draw(vaoVboLst, getVertexCount());
    return std::shared_ptr<ElementForm>(new ElementForm(speed,texture,lightMaterial, vaoVboLst,tupleLst));
}

void ElementForm::uBindBuffers()
{
    for (int i = 0;i< vaoVboLst.size(); i++){
        if (i<vaoVboLst.size()/2)glDeleteBuffers(1,&vaoVboLst[i]);
        else glDeleteVertexArrays(1,&vaoVboLst[i]); 
    }
}

void ElementForm::uDraw(float time, std::shared_ptr<std::map<std::string, GLint>> uMap,
            const glm::vec3 &posCam,
            const glm::mat4 &ProjMatrix, 
            const glm::mat4 &MVMatrix, 
            const glm::mat4 &NormalMatrix, 
            const glm::mat4 &deplacementGeneral,
            LightSource &lightSrc)
{
             for (auto i=0;i<tupleList.size(); i++){
                auto mTransfo = std::get<2>(tupleList[i])(speed,time);
                auto center = glm::vec4(0,0,0,1);
                center = deplacementGeneral*mTransfo*center;
                lightMaterial->uSend(uMap);
                lightSrc.uSend(uMap, posCam, glm::vec3(center.x,center.y,center.z));

                glUniform1i((*uMap)["uTextureArray"], 0); // Array Uniform
                glUniform1f((*uMap)["uApplyLight"], true); // Array Uniform

                glUniformMatrix4fv((*uMap)["uMVPMatrix"], 1,GL_FALSE, glm::value_ptr(ProjMatrix*MVMatrix*deplacementGeneral*mTransfo));
                glUniformMatrix4fv((*uMap)["uMVMatrix"], 1,GL_FALSE, glm::value_ptr(MVMatrix * deplacementGeneral*mTransfo));
                glUniformMatrix4fv((*uMap)["uNormalMatrix"], 1,GL_FALSE, glm::value_ptr(NormalMatrix*deplacementGeneral));
                glBindVertexArray(std::get<0>(tupleList[i]));
                glBindTexture(GL_TEXTURE_2D_ARRAY, texture);
                glDrawArrays(GL_TRIANGLES, 0, std::get<1>(tupleList[i]));
                glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
                glBindVertexArray(0);
             }
}

}