#include "glimac/Functions.hpp"

namespace glimac {

void fuseData(std::vector<float>& data,const ShapeVertex* ptr, int VertexCount, glm::mat4 transfo){
    for (int i = 0; i< VertexCount; i++){
        /*
            glm::vec3 position;
            glm::vec3 normal;
            glm::vec2 texCoords;
            GLfloat texture;
        */
        glm::vec4 vertexPosition = glm::vec4(ptr[i].position, 1);
        auto pos = glm::vec3(transfo * vertexPosition);
        data.push_back(pos.x);
        data.push_back(pos.y);
        data.push_back(pos.z);

        data.push_back(ptr[i].normal.x);
        data.push_back(ptr[i].normal.y);
        data.push_back(ptr[i].normal.z);

        data.push_back(ptr[i].texCoords.x);
        data.push_back(ptr[i].texCoords.y);
        
        data.push_back(ptr[i].texture);
    }

}


void fuseBigData(float *data, int posData , const ShapeVertex* ptr, int VertexCount, glm::mat4 transfo){
    auto pData = posData;

    for (int i = 0; i< VertexCount; i++){
        /*
            glm::vec3 position;
            glm::vec3 normal;
            glm::vec2 texCoords;
            GLfloat texture;
        */

        glm::vec4 vertexPosition = glm::vec4(ptr[i].position, 1);
        auto pos = glm::vec3(transfo * vertexPosition);
        data[pData] = (pos.x);
        data[pData+1] = (pos.y);
        data[pData+2] = (pos.z);

        data[pData+3] = (ptr[i].normal.x);
        data[pData+4] = (ptr[i].normal.y);
        data[pData+5] = (ptr[i].normal.z);

        data[pData+6] = (ptr[i].texCoords.x);
        data[pData+7] = (ptr[i].texCoords.y);
        
        data[pData+8] = (ptr[i].texture);
        pData += 9;
    }

}
std::shared_ptr<std::map<std::string, GLint>> initUniformeVar (Program &program){
  std::shared_ptr<std::map<std::string, GLint>> map(new std::map<std::string, GLint>());

  GLint uMVPMatrix = glGetUniformLocation(program.getGLId() , "uMVPMatrix");
  GLint uMVMatrix = glGetUniformLocation(program.getGLId() , "uMVMatrix");
  GLint uNormalMatrix = glGetUniformLocation(program.getGLId() , "uNormalMatrix");
  GLint uTexture = glGetUniformLocation(program.getGLId() , "uTexture");
  GLint uTextureArray = glGetUniformLocation(program.getGLId() , "uTextureArray");

  //Obtiention des variables uniformes pour la lumière
  GLint uKd = glGetUniformLocation(program.getGLId(), "uKd");
  GLint uKs = glGetUniformLocation(program.getGLId(), "uKs");
  GLint uShininess = glGetUniformLocation(program.getGLId(), "uShininess");
  GLint uLightDir_vs = glGetUniformLocation(program.getGLId(), "uLightDir_vs");
  GLint uLightIntensity = glGetUniformLocation(program.getGLId(), "uLightIntensity");

  GLint uAmbientLight = glGetUniformLocation(program.getGLId(), "uAmbientLight");
  GLint uApplyLight = glGetUniformLocation(program.getGLId(), "uApplyLight");
  GLint uPosCam = glGetUniformLocation(program.getGLId(), "uPosCam");
  GLint uCenter = glGetUniformLocation(program.getGLId(), "uCenter");

  map->insert(std::pair<std::string,GLint>("uMVPMatrix",uMVPMatrix));
  map->insert(std::pair<std::string,GLint>("uMVMatrix",uMVMatrix));
  map->insert(std::pair<std::string,GLint>("uNormalMatrix",uNormalMatrix));
  map->insert(std::pair<std::string,GLint>("uTexture",uTexture));
  map->insert(std::pair<std::string,GLint>("uTextureArray",uTextureArray));

  map->insert(std::pair<std::string,GLint>("uKd",uKd));
  map->insert(std::pair<std::string,GLint>("uKs",uKs));
  map->insert(std::pair<std::string,GLint>("uShininess",uShininess));
  map->insert(std::pair<std::string,GLint>("uLightDir_vs",uLightDir_vs));
  map->insert(std::pair<std::string,GLint>("uLightIntensity",uLightIntensity));

  map->insert(std::pair<std::string,GLint>("uAmbientLight",uAmbientLight));
  map->insert(std::pair<std::string,GLint>("uApplyLight",uApplyLight));
  map->insert(std::pair<std::string,GLint>("uPosCam",uPosCam));
  map->insert(std::pair<std::string,GLint>("uCenter",uCenter));
  return map;
}

std::map<std::string, GLuint> initUniformTex(GLuint *&texture, GLuint *&arrayTexture,const std::string &path){
  texture = new GLuint[3];
  arrayTexture = new GLuint[2];
  glActiveTexture(GL_TEXTURE0);
    std::vector<std::unique_ptr<Image>> pPicture;
    pPicture.push_back( loadImage(path + "/textures/MoonMap.jpg"));
    pPicture.push_back( loadImage(path + "/textures/Sun.jpg"));
    pPicture.push_back( loadImage(path + "/textures/SkyNight.jpg"));
    //pPicture.push_back( loadImage("../../assets/textures/Sky.jpg"));
    // bind list texture
    //bindLstTexture(pPicture,texture,4);

  std::map<std::string, GLuint> uNameText;

  bindMapTexture((pPicture[0]).get(), &(texture[0]), pPicture[0]->getWidth(), pPicture[0]->getHeight());
  bindMapTexture((pPicture[1]).get(), &(texture[1]), pPicture[1]->getWidth(), pPicture[1]->getHeight());
  bindMapTexture((pPicture[2]).get(), &(texture[2]), pPicture[2]->getWidth(), pPicture[2]->getHeight());
  //bindMapTexture((pPicture[3]).get(), &(texture[3]), pPicture[3]->getWidth(), pPicture[3]->getHeight());
  uNameText.insert(std::pair<std::string,GLuint>("Moon",texture[0]));
  uNameText.insert(std::pair<std::string,GLuint>("Sun",texture[1]));
  uNameText.insert(std::pair<std::string,GLuint>("SkyNight",texture[2]));
  //uNameText.insert(std::pair<std::string,GLuint>("Sky",texture[3]));

  /********* Array Texture *********/
  std::unique_ptr<Image> mapAtlasTerrain  = loadImage(path + "/textures/Terrain.png");
  bindMapTexture(mapAtlasTerrain.get(), &(arrayTexture[0]), 16, 16, 256 );
  uNameText.insert(std::pair<std::string,GLuint>("MapAtlas",arrayTexture[0]));

  mapAtlasTerrain  = loadImage(path + "/textures/Sky.png");
  bindMapTexture(mapAtlasTerrain.get(), &(arrayTexture[1]), 480, 480, 3);
  uNameText.insert(std::pair<std::string,GLuint>("Sky",arrayTexture[1]));
  return uNameText;
}

void freeTex(GLuint *&texture, GLuint *&arrayTexture){
    glDeleteTextures(4,texture);
    glDeleteTextures(1,arrayTexture);
    delete (texture);
    delete (arrayTexture);

}

}