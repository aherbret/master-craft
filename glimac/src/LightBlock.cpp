#include "glimac/LightBlock.hpp"

namespace glimac {
	void LightBlock::uSend(std::shared_ptr<std::map<std::string, GLint>> uMap){
	      glUniform3f((*uMap)["uKd"], Kd.x, Kd.y, Kd.z);
	      glUniform3f((*uMap)["uKs"], Ks.x, Ks.y, Ks.z);
	      glUniform1f((*uMap)["uShininess"], Shininess);
	}
	void LightSource::uSend(std::shared_ptr<std::map<std::string, GLint>> uMap,const glm::vec3 &PosCam,const glm::vec3 &Center){
          glUniform3f((*uMap)["uPosCam"], PosCam.x, PosCam.y, PosCam.z);
          glUniform3f((*uMap)["uLightDir_vs"], LightDir_vs.x, LightDir_vs.y, LightDir_vs.z);
          glUniform3f((*uMap)["uLightIntensity"], LightIntensity.x, LightIntensity.y, LightIntensity.z);
          glUniform3f((*uMap)["uAmbientLight"], AmbientLight.x, AmbientLight.y, AmbientLight.z);
          glUniform3f((*uMap)["uCenter"], Center.x, Center.y, Center.z);
	}
	void LightSource::uSendAmbientLight(std::shared_ptr<std::map<std::string, GLint>> uMap){
          glUniform3f((*uMap)["uAmbientLight"], AmbientLight.x, AmbientLight.y, AmbientLight.z);
	}

	void LightState::uSend(std::shared_ptr<std::map<std::string, GLint>> uMap){
		glUniform1i((*uMap)["uTextureArray"], TextureArray); // Array Uniform
		glUniform1f((*uMap)["uApplyLight"], ApplyLight); // Array Uniform
	}

}