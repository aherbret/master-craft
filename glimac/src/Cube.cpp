#include <cmath>
#include <vector>
#include <iostream>
#include "glimac/common.hpp"
#include "glimac/Cube.hpp"

namespace glimac {

void Cube::build(GLfloat front, GLfloat up, GLfloat down) {

    // struct ShapeVertex {
    //     glm::vec3 position;
    //     glm::vec3 normal;
    //     glm::vec2 texCoords;

    //     ShapeVertex(){}
    //     ShapeVertex(glm::vec3 position, glm::vec3 normal, glm::vec2 texCoords):position(position), normal(normal), texCoords(texCoords){}
    // };
    std::vector<ShapeVertex> data;

      ShapeVertex vertex;

      // Texcoord
      glm::vec2 vertexUpLeft;
      glm::vec2 vertexUpRight;
      glm::vec2 vertexDownLeft;
      glm::vec2 vertexDownRight;

      vertexUpLeft.x = 0;
      vertexUpLeft.y = 0;

      vertexUpRight.x = 1;
      vertexUpRight.y = 0;

      vertexDownLeft.x = 0;
      vertexDownLeft.y = 1;

      vertexDownRight.x = 1;
      vertexDownRight.y = 1;

      // 

    //Face 1
        //Triangle 1
            //ShapeVertex(glm::vec3(-1., 1., 1.), glm::vec3(-1., 1., 1.), glm::vec2(0., 1.)),
            vertex.position.x = -1;
            vertex.position.y = 1;
            vertex.position.z = 1;

            vertex.normal.x = 0;
            vertex.normal.y = 0;
            vertex.normal.z = 1;
            vertex.texCoords = vertexUpLeft;
            vertex.texture = front;
            data.push_back(vertex);
            
            //ShapeVertex(glm::vec3(-1., -1., 1.), glm::vec3(-1., -1., 1.), glm::vec2(0., 0.)),
            vertex.position.x = -1;
            vertex.position.y = -1;
            vertex.position.z = 1;

            vertex.normal.x = 0;
            vertex.normal.y = 0;
            vertex.normal.z = 1;

            vertex.texCoords = vertexDownLeft;
            vertex.texture = front;
            data.push_back(vertex);
            
            //ShapeVertex(glm::vec3(1., 1., 1.), glm::vec3(1., 1., 1.), glm::vec2(1., 1.)),
            vertex.position.x = 1;
            vertex.position.y = 1;
            vertex.position.z = 1;

            vertex.normal.x = 0;
            vertex.normal.y = 0;
            vertex.normal.z = 1;

            vertex.texCoords = vertexUpRight;
            vertex.texture = front;
            data.push_back(vertex);
        //Triangle 2
            //ShapeVertex(glm::vec3(-1., -1., 1.), glm::vec3(-1., -1., 1.), glm::vec2(0., 0.)),
            vertex.position.x = -1;
            vertex.position.y = -1;
            vertex.position.z = 1;

            vertex.normal.x = 0;
            vertex.normal.y = 0;
            vertex.normal.z = 1;

            vertex.texCoords = vertexDownLeft;
            vertex.texture = front;
            data.push_back(vertex);
            
            //ShapeVertex(glm::vec3(1., 1., 1.), glm::vec3(1., 1., 1.), glm::vec2(1., 1.)),
            vertex.position.x = 1;
            vertex.position.y = 1;
            vertex.position.z = 1;

            vertex.normal.x = 0;
            vertex.normal.y = 0;
            vertex.normal.z = 1;

            vertex.texCoords = vertexUpRight;
            vertex.texture = front;
            data.push_back(vertex);
            
            //ShapeVertex(glm::vec3(1., -1., 1.), glm::vec3(1., -1., 1.), glm::vec2(1., 0.)),
            vertex.position.x = 1;
            vertex.position.y = -1;
            vertex.position.z = 1;
            
            vertex.normal.x = 0;
            vertex.normal.y = 0;
            vertex.normal.z = 1;
            
            vertex.texCoords = vertexDownRight;
            vertex.texture = front;
            data.push_back(vertex);
    //Face 2
        //Triangle 1
            //ShapeVertex(glm::vec3(1., 1., 1.), glm::vec3(1., 1., 1.), glm::vec2(1., 1.)),
            vertex.position.x = 1;
            vertex.position.y = 1;
            vertex.position.z = 1;
            
            vertex.normal.x = 1;
            vertex.normal.y = 0;
            vertex.normal.z = 0;
            
            
            vertex.texCoords = vertexUpLeft;
            vertex.texture = front;
            data.push_back(vertex);
            
            //ShapeVertex(glm::vec3(1., -1., 1.), glm::vec3(1., -1., 1.), glm::vec2(1., 0.)),
            vertex.position.x = 1;
            vertex.position.y = -1;
            vertex.position.z = 1;
            
            vertex.normal.x = 1;
            vertex.normal.y = 0;
            vertex.normal.z = 0;
            
            
            vertex.texCoords = vertexDownLeft;
            vertex.texture = front;
            data.push_back(vertex);
            
            //ShapeVertex(glm::vec3(1., 1., -1.), glm::vec3(1., 1., -1.), glm::vec2(1., 1.)),
            vertex.position.x = 1;
            vertex.position.y = 1;
            vertex.position.z = -1;
            
            vertex.normal.x = 1;
            vertex.normal.y = 0;
            vertex.normal.z = 0;
            
            vertex.texCoords = vertexUpRight;
            vertex.texture = front;
            data.push_back(vertex);
        //Triangle 2
            //ShapeVertex(glm::vec3(1., -1., 1.), glm::vec3(1., -1., 1.), glm::vec2(1., 0.)),
            vertex.position.x = 1;
            vertex.position.y = -1;
            vertex.position.z = 1;
            
            vertex.normal.x = 1;
            vertex.normal.y = 0;
            vertex.normal.z = 0;
            
            
            vertex.texCoords = vertexDownLeft;
            vertex.texture = front;
            data.push_back(vertex);
            
            //ShapeVertex(glm::vec3(1., 1., -1.), glm::vec3(1., 1., -1.), glm::vec2(1., 1.)),
            vertex.position.x = 1;
            vertex.position.y = 1;
            vertex.position.z = -1;
            
            vertex.normal.x = 1;
            vertex.normal.y = 0;
            vertex.normal.z = 0;
            
            
            vertex.texCoords = vertexUpRight;
            vertex.texture = front;
            data.push_back(vertex);

            //ShapeVertex(glm::vec3(1., -1., -1.), glm::vec3(1., -1., -1.), glm::vec2(1., 0.)),
            vertex.position.x = 1;
            vertex.position.y = -1;
            vertex.position.z = -1;
            
            vertex.normal.x = 1;
            vertex.normal.y = 0;
            vertex.normal.z = 0;
            
            
            vertex.texCoords = vertexDownRight;
            vertex.texture = front;
            data.push_back(vertex);
    //Face 3
        //Triangle 1
        //ShapeVertex(glm::vec3(1., 1., -1.), glm::vec3(1., 1., -1.), glm::vec2(1., 1.)),
            vertex.position.x = 1;
            vertex.position.y = 1;
            vertex.position.z = -1;
            
            vertex.normal.x = 0;
            vertex.normal.y = 0;
            vertex.normal.z = -1;
            
            vertex.texCoords = vertexUpLeft;
            vertex.texture = front;
            data.push_back(vertex);

            //ShapeVertex(glm::vec3(1., -1., -1.), glm::vec3(1., -1., -1.), glm::vec2(1., 0.)),
            vertex.position.x = 1;
            vertex.position.y = -1;
            vertex.position.z = -1;
            
            vertex.normal.x = 0;
            vertex.normal.y = 0;
            vertex.normal.z = -1;
            
            vertex.texCoords = vertexDownLeft;
            vertex.texture = front;
            data.push_back(vertex);
            
            //ShapeVertex(glm::vec3(-1., 1., -1.), glm::vec3(-1., 1., -1.), glm::vec2(0., 1.)),
            vertex.position.x = -1;
            vertex.position.y = 1;
            vertex.position.z = -1;
            
            vertex.normal.x = 0;
            vertex.normal.y = 0;
            vertex.normal.z = -1;
            
            vertex.texCoords = vertexUpRight;
            vertex.texture = front;
            data.push_back(vertex);
        //Triangle 2
            //ShapeVertex(glm::vec3(1., -1., -1.), glm::vec3(1., -1., -1.), glm::vec2(1., 0.)),
            vertex.position.x = 1;
            vertex.position.y = -1;
            vertex.position.z = -1;
            
            vertex.normal.x = 0;
            vertex.normal.y = 0;
            vertex.normal.z = -1;
            
            vertex.texCoords = vertexDownLeft;
            vertex.texture = front;
            data.push_back(vertex);
            
            //ShapeVertex(glm::vec3(-1., 1., -1.), glm::vec3(-1., 1., -1.), glm::vec2(0., 1.)),
            vertex.position.x = -1;
            vertex.position.y = 1;
            vertex.position.z = -1;
            
            vertex.normal.x = 0;
            vertex.normal.y = 0;
            vertex.normal.z = -1;
            
            vertex.texCoords = vertexUpRight;
            vertex.texture = front;
            data.push_back(vertex);
            
            //ShapeVertex(glm::vec3(-1., -1., -1.), glm::vec3(-1., -1., -1.), glm::vec2(0., 0.)),
            vertex.position.x = -1;
            vertex.position.y = -1;
            vertex.position.z = -1;
            
            vertex.normal.x = 0;
            vertex.normal.y = 0;
            vertex.normal.z = -1;
            
            vertex.texCoords = vertexDownRight;
            vertex.texture = front;
            data.push_back(vertex);
    //Face 4
        //Triangle 1
        //ShapeVertex(glm::vec3(-1., 1., -1.), glm::vec3(-1., 1., -1.), glm::vec2(0., 1.)),
            vertex.position.x = -1;
            vertex.position.y = 1;
            vertex.position.z = -1;
            
            vertex.normal.x = -1;
            vertex.normal.y = 0;
            vertex.normal.z = 0;
            
            vertex.texCoords = vertexUpLeft;
            vertex.texture = front;
            data.push_back(vertex);
            
            //ShapeVertex(glm::vec3(-1., -1., -1.), glm::vec3(-1., -1., -1.), glm::vec2(0., 0.)),
            vertex.position.x = -1;
            vertex.position.y = -1;
            vertex.position.z = -1;
            
            vertex.normal.x = -1;
            vertex.normal.y = 0;
            vertex.normal.z = 0;
            
            vertex.texCoords = vertexDownLeft;
            vertex.texture = front;
            data.push_back(vertex);
        
            //ShapeVertex(glm::vec3(-1., 1., 1.), glm::vec3(-1., 1., 1.), glm::vec2(0., 1.)),
            vertex.position.x = -1;
            vertex.position.y = 1;
            vertex.position.z = 1;
            
            vertex.normal.x = -1;
            vertex.normal.y = 0;
            vertex.normal.z = 0;
            
            vertex.texCoords = vertexUpRight;
            vertex.texture = front;
            data.push_back(vertex);
        //Triangle 2
        //ShapeVertex(glm::vec3(-1., -1., -1.), glm::vec3(-1., -1., -1.), glm::vec2(0., 0.)),
            vertex.position.x = -1;
            vertex.position.y = -1;
            vertex.position.z = -1;
            
            vertex.normal.x = -1;
            vertex.normal.y = 0;
            vertex.normal.z = 0;
            
            vertex.texCoords = vertexDownLeft;
            vertex.texture = front;
            data.push_back(vertex);
        
            //ShapeVertex(glm::vec3(-1., 1., 1.), glm::vec3(-1., 1., 1.), glm::vec2(0., 1.)),
            vertex.position.x = -1;
            vertex.position.y = 1;
            vertex.position.z = 1;
            
            vertex.normal.x = -1;
            vertex.normal.y = 0;
            vertex.normal.z = 0;
            
            vertex.texCoords = vertexUpRight;
            vertex.texture = front;
            data.push_back(vertex);
        
            //ShapeVertex(glm::vec3(-1., -1., 1.), glm::vec3(-1., -1., 1.), glm::vec2(0., 0.)),
            vertex.position.x = -1;
            vertex.position.y = -1;
            vertex.position.z = 1;
            
            vertex.normal.x = -1;
            vertex.normal.y = 0;
            vertex.normal.z = 0;
            
            vertex.texCoords = vertexDownRight;
            vertex.texture = front;
            data.push_back(vertex);
    //Face 5 (up)
        //Triangle 1
            //ShapeVertex(glm::vec3(-1., 1., -1.), glm::vec3(-1., 1., -1.), glm::vec2(0., 1.)),
            vertex.position.x = -1;
            vertex.position.y = 1;
            vertex.position.z = -1;
            
            vertex.normal.x = 0;
            vertex.normal.y = 1;
            vertex.normal.z = 0;
            
            vertex.texCoords = vertexUpLeft;
            vertex.texture = up;
            data.push_back(vertex);

            //ShapeVertex(glm::vec3(-1., 1., 1.), glm::vec3(-1., 1., 1.), glm::vec2(0., 1.)),
            vertex.position.x = -1;
            vertex.position.y = 1;
            vertex.position.z = 1;
            
            vertex.normal.x = 0;
            vertex.normal.y = 1;
            vertex.normal.z = 0;
            
            vertex.texCoords = vertexDownLeft;
            vertex.texture = up;
            data.push_back(vertex);
        
            //ShapeVertex(glm::vec3(1., 1., -1.), glm::vec3(1., 1., -1.), glm::vec2(1., 1.)),
            vertex.position.x = 1;
            vertex.position.y = 1;
            vertex.position.z = -1;
            
            vertex.normal.x = 0;
            vertex.normal.y = 1;
            vertex.normal.z = 0;
            
            vertex.texCoords = vertexUpRight;
            vertex.texture = up;
            data.push_back(vertex);
        //Triangle 2
        //ShapeVertex(glm::vec3(-1., 1., 1.), glm::vec3(-1., 1., 1.), glm::vec2(0., 1.)),
            vertex.position.x = -1;
            vertex.position.y = 1;
            vertex.position.z = 1;
            
            vertex.normal.x = 0;
            vertex.normal.y = 1;
            vertex.normal.z = 0;
            
            vertex.texCoords = vertexDownLeft;
            vertex.texture = up;
            data.push_back(vertex);
        
            //ShapeVertex(glm::vec3(1., 1., -1.), glm::vec3(1., 1., -1.), glm::vec2(1., 1.)),
            vertex.position.x = 1;
            vertex.position.y = 1;
            vertex.position.z = -1;
            
            vertex.normal.x = 0;
            vertex.normal.y = 1;
            vertex.normal.z = 0;
            
            vertex.texCoords = vertexUpRight;
            vertex.texture = up;
            data.push_back(vertex);
        
            //ShapeVertex(glm::vec3(1., 1., 1.), glm::vec3(1., 1., 1.), glm::vec2(1., 1.)),
            vertex.position.x = 1;
            vertex.position.y = 1;
            vertex.position.z = 1;
            
            vertex.normal.x = 0;
            vertex.normal.y = 1;
            vertex.normal.z = 0;
            
            vertex.texCoords = vertexDownRight;
            vertex.texture = up;
            data.push_back(vertex);
    //Face 6 (bottom)
        //Triangle 1
            //ShapeVertex(glm::vec3(-1., -1., -1.), glm::vec3(-1., -1., -1.), glm::vec2(0., 0.)),
            vertex.position.x = -1;
            vertex.position.y = -1;
            vertex.position.z = -1;
            
            vertex.normal.x = 0;
            vertex.normal.y = -1;
            vertex.normal.z = 0;
            
            vertex.texCoords = vertexUpLeft;
            vertex.texture = down;
            data.push_back(vertex);

            //ShapeVertex(glm::vec3(-1., -1., 1.), glm::vec3(-1., -1., 1.), glm::vec2(0., 0.)),
            vertex.position.x = -1;
            vertex.position.y = -1;
            vertex.position.z = 1;
            
            vertex.normal.x = 0;
            vertex.normal.y = -1;
            vertex.normal.z = 0;
            
            vertex.texCoords = vertexDownLeft;
            vertex.texture = down;
            data.push_back(vertex);

            //ShapeVertex(glm::vec3(1., -1., -1.), glm::vec3(1., -1., -1.), glm::vec2(1., 0.)),
            vertex.position.x = 1;
            vertex.position.y = -1;
            vertex.position.z = -1;
            
            vertex.normal.x = 0;
            vertex.normal.y = -1;
            vertex.normal.z = 0;
            
            vertex.texCoords = vertexUpRight;
            vertex.texture = down;
            data.push_back(vertex);
        //Triangle 2
            //ShapeVertex(glm::vec3(-1., -1., 1.), glm::vec3(-1., -1., 1.), glm::vec2(0., 0.)),
            vertex.position.x = -1;
            vertex.position.y = -1;
            vertex.position.z = 1;
            
            vertex.normal.x = 0;
            vertex.normal.y = -1;
            vertex.normal.z = 0;
            
            vertex.texCoords = vertexDownLeft;
            vertex.texture = down;
            data.push_back(vertex);

            //ShapeVertex(glm::vec3(1., -1., -1.), glm::vec3(1., -1., -1.), glm::vec2(1., 0.)),
            vertex.position.x = 1;
            vertex.position.y = -1;
            vertex.position.z = -1;
            
            vertex.normal.x = 0;
            vertex.normal.y = -1;
            vertex.normal.z = 0;
            
            vertex.texCoords = vertexUpRight;
            vertex.texture = down;
            data.push_back(vertex);
        
            //ShapeVertex(glm::vec3(1., -1., 1.), glm::vec3(1., -1., 1.), glm::vec2(1., 0.))
            vertex.position.x = 1;
            vertex.position.y = -1;
            vertex.position.z = 1;
            
            vertex.normal.x = 0;
            vertex.normal.y = -1;
            vertex.normal.z = 0;
            
            vertex.texCoords = vertexDownRight;
            vertex.texture = down;
            data.push_back(vertex);

    m_Vertices = data;
    m_nVertexCount = 36;
}

}
