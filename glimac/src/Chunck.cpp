#include "glimac/Chunck.hpp"

namespace glimac {
int findFirstElmVector(std::vector<glm::vec3> typeCubeColor, glm::vec3 Color){
    for (auto i = 0 ; i< typeCubeColor.size(); i++){
        if (typeCubeColor[i] == Color){
            return i;
        }
    }
    return -1;
}


int indexPos(int x, int z,int width,int height) {// From DM to DM
    auto posX = x+(int)(width/2.);
    auto posZ = z+(int)(height/2.);
    if (posX < 0 || posX >= width){
      throw std::range_error("X outside of the map");
    }
    if (posZ < 0 || posZ >= height){
      throw std::range_error("Z outside of the map");
    }
    return posX+posZ*width;
    }

std::shared_ptr<ElementForm> Chunck::generateMapElementForm(const DeepMap &deepMap, const std::vector<glm::vec3> &hColorMap, const std::vector<glm::vec3> &typeCubeColor, const std::vector<Cube> &typeCube, int xPos, int zPos,int sizeChunckX, int sizeChunckZ,
            GLuint texture,
            LightBlock *lightMaterial,
            int VERTEX_ATTR_POSITION, 
            int VERTEX_ATTR_NORMAL, 
            int VERTEX_ATTR_TEXCOORDS, 
            int VERTEX_ATTR_TEXTURE
            ){

                /*Color of the cube*/

                /*Creation of the chunck*/
                int nb_block = ((sizeChunckX*sizeChunckZ*256))*typeCube[0].getVertexCount()*9; // nb tt de float
                float* data = new float[nb_block];
                int posData = 0;
                int index = 0;
                int widthDM = (deepMap.getWidth()/2.);
                int heightDM = (deepMap.getHeight()/2.);
                
                auto isOutside = [=](int x, int z){
                    //std::cout << "Data " << x << " " << z << "  ||   " <<widthDM << " " << heightDM << std::endl;
                    if( (widthDM <= x) || (heightDM <= z) ){
                        //std::cout <<"Out of the map" << x << " " << z << std::endl; 
                        return true;
                    }
                    else if( (-widthDM > x) || (-heightDM > z) ){
                        //std::cout <<"Out of the map" << x << " " << z << std::endl;
                        return true;
                    }    
                    return false;                                
                };
                omp_set_num_threads(sizeChunckX);
                #pragma omp parallel for
                for( auto i = 0; i < (sizeChunckX*sizeChunckZ); i++) {
                        int h;
                        int up;
                        int down;
                        int left;
                        int right;
                        auto idx = -1;//
                  //for( auto i = 0; i < (sizeChunckX*sizeChunckZ); i++) {
                        int x = (i)%sizeChunckX;// coordonnée DeepMap
                        int z = (i)/sizeChunckX;// coordonnée DeepMap
                        // xPos et zPos coordonnée Chunck
                        int posXMap = (x+xPos*sizeChunckX)*2;
                        int posZMap = (z+zPos*sizeChunckZ)*2;
                        if (isOutside(x+xPos*sizeChunckX, z+zPos*sizeChunckZ))h = -256;
                        else {
                            h = deepMap.getGround(posXMap, posZMap);
                            idx = findFirstElmVector(typeCubeColor, hColorMap[indexPos((x+xPos*sizeChunckX), (z+zPos*sizeChunckZ),deepMap.getWidth() ,deepMap.getHeight())]);
                        }
                        // Trouvé le min de cette pos pour empiler
                        if (isOutside(x+xPos*sizeChunckX, z+zPos*sizeChunckZ+1))up = -256;
                        else up = deepMap.getGround(posXMap, posZMap+2);

                        if (isOutside(x+xPos*sizeChunckX, z+zPos*sizeChunckZ-1))down = -256;
                        else down = deepMap.getGround(posXMap, posZMap-2);

                        if (isOutside(x+xPos*sizeChunckX-1, z+zPos*sizeChunckZ))left = -256;
                        else left = deepMap.getGround(posXMap-2, posZMap);

                        if (isOutside(x+xPos*sizeChunckX+1, z+zPos*sizeChunckZ))right = -256;
                        else right = deepMap.getGround(posXMap+2, posZMap);
                        
                        int minSize = std::min(up,std::min(down,std::min(left,right)));                             
                        //std::cout << "Color " << idx << " " << hColorMap[indexPos((x+xPos*sizeChunckX), (z+zPos*sizeChunckZ),deepMap.getWidth() ,deepMap.getHeight())]  << std::endl;
                        if (idx >= 0){
                        auto cubeType = typeCube[idx];
                        auto cubeTypeFiller = typeCube[idx+1];
                        auto tmp = glm::translate(glm::mat4(1) ,glm::vec3(x*2, h, z*2));
                        #pragma omp critical
                        {
                            fuseBigData(data,posData ,cubeType.getDataPointer(), cubeType.getVertexCount(),tmp);
                            posData += cubeType.getVertexCount()*9;
                        }
                        for (int y =(int)(minSize/2.); y< (int)(h/2.); y++){
                               //std::cout << "Data h : " << h/2 << " " << x+xPos*sizeChunckX<< " " << z+zPos*sizeChunckZ<< std::endl; 

                                tmp = glm::translate(glm::mat4(1) ,glm::vec3(x*2, y*2, z*2));
                                #pragma omp critical
                                {
                                    fuseBigData(data,posData ,cubeTypeFiller.getDataPointer(), cubeTypeFiller.getVertexCount(),tmp);
                                    posData += cubeTypeFiller.getVertexCount()*9;
                                }
                        }

                        //if (xPos == -1 && zPos == -1)
                        //std::cout <<" Ground " << deepMap.getGround(-25,-25) << " Ground from XY" << deepMap.getGround(posXMap, posZMap) << " x " << x+xPos*sizeChunckX<< " z " << z+zPos*sizeChunckZ<< std::endl;
                        }
                 }


                auto bind = [=](const std::vector<GLuint> &vaoVboLst,const std::vector<int> &vertexCount){
                      if (vaoVboLst.size() != 2){
                        throw std::length_error("Illegal state exeption, vaoVboLst size != 4");
                      }
                      std::vector<std::tuple<GLuint, int, std::function<glm::mat4(double,double)>>> map;
                      // speed time
                      std::function<glm::mat4(double,double)> fcBody = [](float speed, float time){return glm::mat4(1);};
                      map.push_back(std::make_tuple(vaoVboLst[0] ,vertexCount[0], fcBody));
                      return map;
                    };

                auto draw = [=](int VERTEX_ATTR_POSITION, int VERTEX_ATTR_NORMAL, int VERTEX_ATTR_TEXCOORDS, int VERTEX_ATTR_TEXTURE){
                            std::vector<GLuint> vaoVboLst;
                            //// Feuille
                            GLuint vboDecoMap;
                            glGenBuffers(1, &vboDecoMap);
                            glBindBuffer(GL_ARRAY_BUFFER,vboDecoMap);
                            /************ Calcul map *************/
                            /*************************/
                            glBufferData(GL_ARRAY_BUFFER, posData* sizeof(float) , data,GL_STATIC_DRAW);
                            //glBufferData(GL_ARRAY_BUFFER, map.getVertexCount()*sizeof(ShapeVertex) , map.getDataPointer(),GL_STATIC_DRAW);
                            // debind les info
                            glBindBuffer(GL_ARRAY_BUFFER,0);
                            // description / interpretation des flotant obtenu dans le gpu
                            GLuint vaoDecoMap;
                            glGenVertexArrays(1,&vaoDecoMap);
                            glBindVertexArray(vaoDecoMap);
                            glEnableVertexAttribArray(VERTEX_ATTR_POSITION);
                            glEnableVertexAttribArray(VERTEX_ATTR_NORMAL);
                            glEnableVertexAttribArray(VERTEX_ATTR_TEXCOORDS);
                            glEnableVertexAttribArray(VERTEX_ATTR_TEXTURE);

                            // Lier vbo et vao
                            glBindBuffer(GL_ARRAY_BUFFER,vboDecoMap);
                            glVertexAttribPointer(VERTEX_ATTR_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), 0);
                            glVertexAttribPointer(VERTEX_ATTR_NORMAL, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)(sizeof(glm::vec3)));
                            glVertexAttribPointer(VERTEX_ATTR_TEXCOORDS, 2, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)(sizeof(glm::vec3)+sizeof(glm::vec3)));
                            glVertexAttribPointer(VERTEX_ATTR_TEXTURE, 1, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)(sizeof(glm::vec3)+sizeof(glm::vec3)+sizeof(glm::vec2)));
                            glBindBuffer(GL_ARRAY_BUFFER,0);
                            glBindVertexArray(0); 

                          vaoVboLst.push_back(vaoDecoMap);
                          vaoVboLst.push_back(vboDecoMap);
                          return vaoVboLst;
                    };

                auto getVertexCount = [=](){
                          Cube dirt(3,0,2); // Front Up Down
                          std::vector<int> vertexCount;
                          vertexCount.push_back(posData/9.);
                          return vertexCount;
                };
                auto tmp = ElementForm::CreateElementForm(0, texture, lightMaterial, draw, bind, getVertexCount, VERTEX_ATTR_POSITION, VERTEX_ATTR_NORMAL, VERTEX_ATTR_TEXCOORDS, VERTEX_ATTR_TEXTURE);
        delete data;
        return tmp;                
 }
}