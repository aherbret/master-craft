#include "glimac/TextureMap.hpp"

/*Rangement memoire pour avoir 1 sky au lieu d'une ligne*/
glm::vec4* ImageToMapAtlass(unsigned int width, unsigned int height,unsigned int blockWidth,unsigned int blockHeight ,glm::vec4 *pixels){
	glm::vec4 * image = new glm::vec4[width*height];
	for (auto cols=0; cols< (float)height/(float)blockHeight ; cols++){
		for (auto lines=0; lines< (float)width/(float)blockWidth ; lines++){


			for (auto i=0; i< blockHeight ; i++){
				for (auto j=0; j< blockWidth ; j++){
					int positionMemoirePixel = (cols*blockHeight*width)+(lines*blockWidth);
					int positionMemoireImage = (cols*blockHeight*width)+(lines*blockWidth*blockHeight);
					int posImage = positionMemoireImage + (i*blockWidth)+(j);
					int posPixel = positionMemoirePixel + (i*width)+(j);
					image[posImage] = pixels[posPixel];
					//std::cout << "Info :"<< cols << " " << lines << " " << i << " " << j << std::endl;
					//std::cout << "Pos :" << posImage << " " << posPixel << std::endl;
					//std::cout << "Adr :" << image[posPixel] << " " << pixels[posPixel] << std::endl;
				}

			}

		}
	}
	return image;
}

//(GLsizei width, GLsizei height)= size block image , GLsizei layerCount = nb blockimage
void bindMapTexture(Image* pPicture, GLuint* arrayTexture, GLsizei width, GLsizei height, GLsizei layerCount ){
	glGenTextures(1, arrayTexture);
	glBindTexture(GL_TEXTURE_2D_ARRAY, (*arrayTexture));
	glm::vec4* image = ImageToMapAtlass(pPicture->getWidth(), pPicture->getHeight(), width, height ,pPicture->getPixels());

	glTexImage3D(GL_TEXTURE_2D_ARRAY,
             0,                 // mipmap level
             GL_RGBA8,          // gpu texel format
             width,             // width
             height,            // height
             layerCount,        // depth
             0,                 // border
             GL_RGBA,      // cpu pixel format
             GL_FLOAT,  // cpu pixel coord type
             image);           // pixel data

	glGenerateMipmap(GL_TEXTURE_2D_ARRAY);

	// These parameters have been chosen for Apanga, where I want a
	// pixelated appearance to the textures as they're magnified.

	glTexParameteri(GL_TEXTURE_2D_ARRAY,
	                GL_TEXTURE_MIN_FILTER,
	                GL_NEAREST_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY,
	                GL_TEXTURE_MAG_FILTER,
	                GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAX_LEVEL, 4);
	glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
}

void bindMapTexture(Image* pPicture, GLuint* arrayTexture, GLsizei width, GLsizei height){
	glGenTextures(1, arrayTexture);
	glBindTexture(GL_TEXTURE_2D_ARRAY, (*arrayTexture));
	glm::vec4* image = ImageToMapAtlass(pPicture->getWidth(), pPicture->getHeight(), width, height ,pPicture->getPixels());

	glTexImage3D(GL_TEXTURE_2D_ARRAY,
             0,                 // mipmap level
             GL_RGBA8,          // gpu texel format
             pPicture->getWidth(),             // width
             pPicture->getHeight(),            // height
             1,        			// depth
             0,                 // border
             GL_RGBA,      // cpu pixel format
             GL_FLOAT,  // cpu pixel coord type
             pPicture->getPixels());           // pixel data
	glGenerateMipmap(GL_TEXTURE_2D_ARRAY);

	// These parameters have been chosen for Apanga, where I want a
	// pixelated appearance to the textures as they're magnified.

	glTexParameteri(GL_TEXTURE_2D_ARRAY,
	                GL_TEXTURE_MIN_FILTER,
	                GL_NEAREST_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY,
	                GL_TEXTURE_MAG_FILTER,
	                GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAX_LEVEL, 4);
	glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
}