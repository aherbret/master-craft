#include "glimac/FreeFlyCamera.hpp"
#include <iostream>

FreeFlyCamera::FreeFlyCamera(){
	m_Position = glm::vec3(0.0f);
	m_fPhi = M_PI;
	m_fTheta = 0.0;
	computeDirectionVectors();
}

void FreeFlyCamera::computeDirectionVectorsFly(){
	float radPhi = m_fPhi;
	float radTheta = m_fTheta;
	m_FrontVector = glm::vec3(cos(radTheta)*sin(radPhi), sin(radTheta), cos(radTheta)*cos(radPhi));
	m_LeftVector = glm::vec3(sin(radPhi+(M_PI/2.0)), 0, cos(radPhi+(M_PI/2.0)));
	m_UpVector = glm::cross(m_FrontVector, m_LeftVector);
}

void FreeFlyCamera::moveLeftFly(float t){
	m_Position += t * m_LeftVector;
	computeDirectionVectorsFly();
}

void FreeFlyCamera::moveFrontFly(float t){
	m_Position += t * m_FrontVector;
	computeDirectionVectorsFly();
}

void FreeFlyCamera::moveUpFly(float t){
	m_Position += t * m_UpVector;
	computeDirectionVectorsFly();
}

void FreeFlyCamera::rotateLeft(float degrees){
	m_fPhi += glm::radians(degrees);
	if (m_fPhi/2*3.1415926535897932384626433832795 > 1){
		m_fPhi -= 2*3.1415926535897932384626433832795;
	}
	if (m_fPhi/2*3.1415926535897932384626433832795 < -1){
		m_fPhi += 2*3.1415926535897932384626433832795;
	}
	computeDirectionVectorsFly();
}

void FreeFlyCamera::rotateUp(float degrees){
	m_fTheta += glm::radians(degrees);
	if (m_fTheta/2*3.1415926535897932384626433832795 > 1){
		m_fTheta -= 2*3.1415926535897932384626433832795;
	}
	if (m_fTheta/2*3.1415926535897932384626433832795 < -1){
		m_fTheta += 2*3.1415926535897932384626433832795;
	}
	computeDirectionVectorsFly();
}

glm::mat4 FreeFlyCamera::getViewMatrix() const{
	//Point
	glm::vec3 Point = m_Position + m_FrontVector;
	glm::mat4 MV = glm::lookAt(m_Position, m_Position + m_FrontVector, m_UpVector);
	return MV;
}

void FreeFlyCamera::computeDirectionVectors(){
	float radPhi = m_fPhi;
	float radTheta = m_fTheta;
	m_FrontVector = glm::vec3(cos(radTheta)*sin(radPhi), sin(radTheta), cos(radTheta)*cos(radPhi));
	m_LeftVector = glm::vec3(sin(radPhi+(M_PI/2.0)), 0, cos(radPhi+(M_PI/2.0)));
	m_UpVector = glm::cross(m_FrontVector, m_LeftVector);
}


void FreeFlyCamera::moveLeft(float t){
	m_Position += t * glm::vec3(m_LeftVector.x,0,m_LeftVector.z);
	computeDirectionVectors();
}

void FreeFlyCamera::moveFront(float t){
	m_Position += t * glm::vec3(m_FrontVector.x,0,m_FrontVector.z);
	computeDirectionVectors();
}


void FreeFlyCamera::moveUp(float t){
	m_Position += t * glm::vec3(0,1,0);
	computeDirectionVectors();
}

void FreeFlyCamera::setXYZ(glm::vec3 pos){
	auto myPos = getPos();
	auto dist = pos-myPos;
	auto x = dist.x;
	m_Position += x * glm::vec3(1,0,0);
	computeDirectionVectors();

	auto y = dist.y;
	m_Position += y * glm::vec3(0,1,0);
	computeDirectionVectors();

	auto z = dist.z;
	m_Position += z * glm::vec3(0,0,1);
	computeDirectionVectors();
}