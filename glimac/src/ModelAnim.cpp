#include "masterCraft/ModelAnim.hpp"

namespace masterCraft {

/****************************************************************  TntBird  ****************************************************************/
std::vector<std::tuple<GLuint, int, std::function<glm::mat4(double,double)>>>
createTntBird(const std::vector<GLuint> &vaoVboLst,const std::vector<int> &vertexCount){
    std::vector<std::tuple<GLuint, int, std::function<glm::mat4(double,double)>>> tntBirdForm;
    if (vaoVboLst.size() != 4){
        throw std::length_error("Illegal state exeption, vaoVboLst size != 4");
    }
    // speed time
    std::function<glm::mat4(double,double)> fcWingsR = [](float speed, float time){
        float timeWing;
        glm::mat4 mat;
        if ((int)glm::degrees(time*speed)%160 < 80)
            timeWing = 40-((int)glm::degrees(time*speed)%80);
         else if ((int)glm::degrees(time*speed)%160 >= 80)
            timeWing = -40+((int)glm::degrees(time*speed)%80);
         timeWing = glm::radians(timeWing);
         mat = glm::rotate(glm::mat4(1) ,timeWing,glm::vec3(0, 0, 1));
         mat = glm::translate(mat ,glm::vec3(1, 0, 0));
         mat = glm::scale(mat ,glm::vec3(1, 0.1, 1));
         mat = glm::translate(mat ,glm::vec3(1, 0, 0));
         return mat;
    };
    std::function<glm::mat4(double,double)> fcWingsL = [](float speed, float time){
        float timeWing;
        glm::mat4 mat;

        if ((int)glm::degrees(time*speed)%160 < 80)
            timeWing = 40-((int)glm::degrees(time*speed)%80);
         else if ((int)glm::degrees(time*speed)%160 >= 80)
            timeWing = -40+((int)glm::degrees(time*speed)%80);
         timeWing = glm::radians(timeWing);
        mat = glm::scale(glm::mat4(1) ,glm::vec3(1, -1, 1));
         mat = glm::rotate(mat ,timeWing+3.1f,glm::vec3(0, 0, 1));
         mat = glm::translate(mat ,glm::vec3(1, 0, 0));
         mat = glm::scale(mat ,glm::vec3(1, 0.1, 1));
         mat = glm::translate(mat ,glm::vec3(1, 0, 0));

         return mat;
    };

    std::function<glm::mat4(double,double)> fcBody = [](float speed, float time){
         return glm::scale(glm::mat4(1) ,glm::vec3(1, 0.3, 1));
    };

    tntBirdForm.push_back(std::make_tuple(vaoVboLst[0] ,vertexCount[0], fcBody));
    tntBirdForm.push_back(std::make_tuple(vaoVboLst[1] ,vertexCount[1], fcWingsR));
    tntBirdForm.push_back(std::make_tuple(vaoVboLst[1] ,vertexCount[1], fcWingsL));

    return tntBirdForm;
}


// vect sous forme : Vao1 Vao2 ... VaoN Vbo1 Vbo2 ... VboN
std::vector<GLuint> bindTextureTntBird(int VERTEX_ATTR_POSITION, int VERTEX_ATTR_NORMAL, int VERTEX_ATTR_TEXCOORDS, int VERTEX_ATTR_TEXTURE){
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  std::vector<GLuint> vaoVboLst;
    GLuint vboNpc1;
    glGenBuffers(1, &vboNpc1);
    glBindBuffer(GL_ARRAY_BUFFER,vboNpc1);
    // ajout des point du tr
    Cube corps(6,8,6); // Front Up Down
    //Cube feuille(0,0,0); // Front Up Down
    glBufferData(GL_ARRAY_BUFFER, corps.getVertexCount()*sizeof(ShapeVertex) , corps.getDataPointer(),GL_STATIC_DRAW);
    //glBufferData(GL_ARRAY_BUFFER, feuille.getVertexCount()*sizeof(ShapeVertex) , feuille.getDataPointer(),GL_STATIC_DRAW);
    // debind les info
    glBindBuffer(GL_ARRAY_BUFFER,0);
    // description / interpretation des flotant obtenu dans le gpu
    GLuint vaoNpc1;
    glGenVertexArrays(1,&vaoNpc1);
    glBindVertexArray(vaoNpc1);
    glEnableVertexAttribArray(VERTEX_ATTR_POSITION);
    glEnableVertexAttribArray(VERTEX_ATTR_NORMAL);
    glEnableVertexAttribArray(VERTEX_ATTR_TEXCOORDS);
    glEnableVertexAttribArray(VERTEX_ATTR_TEXTURE);

    // Lier vbo et vao
    glBindBuffer(GL_ARRAY_BUFFER,vboNpc1);
    glVertexAttribPointer(VERTEX_ATTR_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), 0);
    glVertexAttribPointer(VERTEX_ATTR_NORMAL, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)(sizeof(glm::vec3)));
    glVertexAttribPointer(VERTEX_ATTR_TEXCOORDS, 2, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)(sizeof(glm::vec3)+sizeof(glm::vec3)));
    glVertexAttribPointer(VERTEX_ATTR_TEXTURE, 1, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)(sizeof(glm::vec3)+sizeof(glm::vec3)+sizeof(glm::vec2)));
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindVertexArray(0); 
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    GLuint vboNpc1Wings;
    glGenBuffers(1, &vboNpc1Wings);
    glBindBuffer(GL_ARRAY_BUFFER,vboNpc1Wings);
    // ajout des point du tr
    Cube wings(6,5,5); // Front Up Down
    //Cube feuille(0,0,0); // Front Up Down
    glBufferData(GL_ARRAY_BUFFER, wings.getVertexCount()*sizeof(ShapeVertex) , wings.getDataPointer(),GL_STATIC_DRAW);
    //glBufferData(GL_ARRAY_BUFFER, feuille.getVertexCount()*sizeof(ShapeVertex) , feuille.getDataPointer(),GL_STATIC_DRAW);
    // debind les info
    glBindBuffer(GL_ARRAY_BUFFER,0);
    // description / interpretation des flotant obtenu dans le gpu
    GLuint vaoNpc1Wings;
    glGenVertexArrays(1,&vaoNpc1Wings);
    glBindVertexArray(vaoNpc1Wings);
    glEnableVertexAttribArray(VERTEX_ATTR_POSITION);
    glEnableVertexAttribArray(VERTEX_ATTR_NORMAL);
    glEnableVertexAttribArray(VERTEX_ATTR_TEXCOORDS);
    glEnableVertexAttribArray(VERTEX_ATTR_TEXTURE);

    // Lier vbo et vao
    glBindBuffer(GL_ARRAY_BUFFER,vboNpc1Wings);
    glVertexAttribPointer(VERTEX_ATTR_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), 0);
    glVertexAttribPointer(VERTEX_ATTR_NORMAL, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)(sizeof(glm::vec3)));
    glVertexAttribPointer(VERTEX_ATTR_TEXCOORDS, 2, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)(sizeof(glm::vec3)+sizeof(glm::vec3)));
    glVertexAttribPointer(VERTEX_ATTR_TEXTURE, 1, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)(sizeof(glm::vec3)+sizeof(glm::vec3)+sizeof(glm::vec2)));
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindVertexArray(0); 
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  vaoVboLst.push_back(vaoNpc1);
  vaoVboLst.push_back(vaoNpc1Wings);
  vaoVboLst.push_back(vboNpc1);
  vaoVboLst.push_back(vboNpc1Wings);
  return vaoVboLst;
}

std::vector<int> getVertexCountTntBird(){
    Cube corps(6,8,6);
    Cube wings(6,5,5);
    std::vector<int> vertexCount;
    vertexCount.push_back(corps.getVertexCount());
    vertexCount.push_back(wings.getVertexCount());
    return vertexCount;
}

/****************************************************************  CitrouSlime  ****************************************************************/

std::vector<std::tuple<GLuint, int, std::function<glm::mat4(double,double)>>>
createCitrouSlime(const std::vector<GLuint> &vaoVboLst,const std::vector<int> &vertexCount){
    std::vector<std::tuple<GLuint, int, std::function<glm::mat4(double,double)>>> citrouSlime;
    if (vaoVboLst.size() != 2){
        throw std::length_error("Illegal state exeption, vaoVboLst size != 2");
    }
    // speed time
    std::function<glm::mat4(double,double)> fcBody = [](float speed, float time){
        glm::mat4 mat(1);
        // val compris entre 0.3 et 0.55
        
        auto lissageAnim = [](float x){return 1.17647058824* x*x + 0.1975;};
        auto scale  = ((cos(time*speed)+1.)/8.)+0.3;
        float periode = (time*speed)/(2*3.14);
        auto scaleConst  = ((cos(time*speed)+1.)/8.)+0.3;
        auto jump = ((cos(time*speed)+1.)/2.);
        auto transl = -0.69;

        periode = periode - (int)periode - 0.5;
        if(periode<0. && scale <0.5){
             scale  = lissageAnim(scale);
             //scaleConst  = ((xCarre(cos(time*speed))+1.)/8.)+0.3;
             //jump = ((xCarre(cos(time*speed))+1.)/2.);
        }   
        
        mat = glm::translate(mat ,glm::vec3(0, transl,0));// pos init
        mat = glm::translate(mat ,glm::vec3(0, scaleConst-0.3,0)); // colle sol
        mat = glm::translate(mat ,glm::vec3(0, jump,0)); // jump
        mat = glm::scale(mat ,glm::vec3(1-scale, scale, 1-scale)); // mov scale
        
        /*Modèle simple*/
        /*
        auto scale = ((sin(time*speed)+1.)/8.)+0.3;
        auto jump = ((sin(time*speed)+1.)/2.);
        auto transl = -0.69;
        mat = glm::translate(mat ,glm::vec3(0, transl,0));// pos init
        mat = glm::translate(mat ,glm::vec3(0, scale-0.3,0)); // colle sol
        mat = glm::translate(mat ,glm::vec3(0, jump,0)); // jump
        mat = glm::scale(mat ,glm::vec3(1-scale, scale, 1-scale)); // mov scale
        */
        return mat;
    };

    citrouSlime.push_back(std::make_tuple(vaoVboLst[0] ,vertexCount[0], fcBody));

    return citrouSlime;
}


std::vector<GLuint> bindTextureCitrouSlime(int VERTEX_ATTR_POSITION, int VERTEX_ATTR_NORMAL, int VERTEX_ATTR_TEXCOORDS, int VERTEX_ATTR_TEXTURE){
  std::vector<GLuint> vaoVboLst;
    GLuint vboNpc1;
    glGenBuffers(1, &vboNpc1);
    glBindBuffer(GL_ARRAY_BUFFER,vboNpc1);
    // ajout des point du tr
    Cube corps(118,102,118); // Front Up Down
    //Cube feuille(0,0,0); // Front Up Down
    glBufferData(GL_ARRAY_BUFFER, corps.getVertexCount()*sizeof(ShapeVertex) , corps.getDataPointer(),GL_STATIC_DRAW);
    //glBufferData(GL_ARRAY_BUFFER, feuille.getVertexCount()*sizeof(ShapeVertex) , feuille.getDataPointer(),GL_STATIC_DRAW);
    // debind les info
    glBindBuffer(GL_ARRAY_BUFFER,0);
    // description / interpretation des flotant obtenu dans le gpu
    GLuint vaoNpc1;
    glGenVertexArrays(1,&vaoNpc1);
    glBindVertexArray(vaoNpc1);
    glEnableVertexAttribArray(VERTEX_ATTR_POSITION);
    glEnableVertexAttribArray(VERTEX_ATTR_NORMAL);
    glEnableVertexAttribArray(VERTEX_ATTR_TEXCOORDS);
    glEnableVertexAttribArray(VERTEX_ATTR_TEXTURE);

    // Lier vbo et vao
    glBindBuffer(GL_ARRAY_BUFFER,vboNpc1);
    glVertexAttribPointer(VERTEX_ATTR_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), 0);
    glVertexAttribPointer(VERTEX_ATTR_NORMAL, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)(sizeof(glm::vec3)));
    glVertexAttribPointer(VERTEX_ATTR_TEXCOORDS, 2, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)(sizeof(glm::vec3)+sizeof(glm::vec3)));
    glVertexAttribPointer(VERTEX_ATTR_TEXTURE, 1, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)(sizeof(glm::vec3)+sizeof(glm::vec3)+sizeof(glm::vec2)));
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindVertexArray(0); 

  vaoVboLst.push_back(vaoNpc1);
  vaoVboLst.push_back(vboNpc1);
  return vaoVboLst;
}

std::vector<int> getVertexCountCitrouSlime(){
    Cube corps(118,102,118); // Front Up Down
    std::vector<int> vertexCount;
    vertexCount.push_back(corps.getVertexCount());
    return vertexCount;
}

void fusion(){

}

/****************************************************************  Cactus  ****************************************************************/
std::vector<std::tuple<GLuint, int, std::function<glm::mat4(double,double)>>>
createCactus(const std::vector<GLuint> &vaoVboLst,const std::vector<int> &vertexCount){
    std::vector<std::tuple<GLuint, int, std::function<glm::mat4(double,double)>>> cactus;
    if (vaoVboLst.size() != 2){
        throw std::length_error("Illegal state exeption, vaoVboLst size != 2");
    }
    // speed time
    std::function<glm::mat4(double,double)> fcBody = [](float speed, float time){
        return glm::mat4(1);
    };
    cactus.push_back(std::make_tuple(vaoVboLst[0] ,vertexCount[0], fcBody));

    return cactus;
}

// opti memoire precalcule dse bloque


std::vector<GLuint> bindTextureCactus(int VERTEX_ATTR_POSITION, int VERTEX_ATTR_NORMAL, int VERTEX_ATTR_TEXCOORDS, int VERTEX_ATTR_TEXTURE){
  std::vector<GLuint> vaoVboLst;
    GLuint vboDeco;
    glGenBuffers(1, &vboDeco);
    glBindBuffer(GL_ARRAY_BUFFER,vboDeco);
    // ajout des point du tr
    Cube corps(70,69,71); // Front Up Down
    //Cube feuille(0,0,0); // Front Up Down
    // ligne de 9 float
    glm::mat4 mat(1);
    mat = glm::translate(mat ,glm::vec3(0, 2,0));// pos init
    std::vector<float> data;
    fuseData(data,corps.getDataPointer(), corps.getVertexCount(),mat);
    fuseData(data,corps.getDataPointer(), corps.getVertexCount(),glm::mat4(1));

    glBufferData(GL_ARRAY_BUFFER, 2*corps.getVertexCount()*sizeof(ShapeVertex) , data.data(),GL_STATIC_DRAW);
    //glBufferData(GL_ARRAY_BUFFER, feuille.getVertexCount()*sizeof(ShapeVertex) , feuille.getDataPointer(),GL_STATIC_DRAW);
    // debind les info
    glBindBuffer(GL_ARRAY_BUFFER,0);
    // description / interpretation des flotant obtenu dans le gpu
    GLuint vaoDeco;
    glGenVertexArrays(1,&vaoDeco);
    glBindVertexArray(vaoDeco);
    glEnableVertexAttribArray(VERTEX_ATTR_POSITION);
    glEnableVertexAttribArray(VERTEX_ATTR_NORMAL);
    glEnableVertexAttribArray(VERTEX_ATTR_TEXCOORDS);
    glEnableVertexAttribArray(VERTEX_ATTR_TEXTURE);

    // Lier vbo et vao
    glBindBuffer(GL_ARRAY_BUFFER,vboDeco);
    glVertexAttribPointer(VERTEX_ATTR_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), 0);
    glVertexAttribPointer(VERTEX_ATTR_NORMAL, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)(sizeof(glm::vec3)));
    glVertexAttribPointer(VERTEX_ATTR_TEXCOORDS, 2, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)(sizeof(glm::vec3)+sizeof(glm::vec3)));
    glVertexAttribPointer(VERTEX_ATTR_TEXTURE, 1, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)(sizeof(glm::vec3)+sizeof(glm::vec3)+sizeof(glm::vec2)));
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindVertexArray(0); 

  vaoVboLst.push_back(vaoDeco);
  vaoVboLst.push_back(vboDeco);
  return vaoVboLst;
}

std::vector<int> getVertexCountCactus(){
    Cube corps(70,69,71); // Front Up Down
    std::vector<int> vertexCount;
    vertexCount.push_back(2*corps.getVertexCount());
    return vertexCount;
}


/****************************************************************  Apple  ****************************************************************/
std::vector<std::tuple<GLuint, int, std::function<glm::mat4(double,double)>>>
createApple(const std::vector<GLuint> &vaoVboLst,const std::vector<int> &vertexCount){
    std::vector<std::tuple<GLuint, int, std::function<glm::mat4(double,double)>>> apple;
    if (vaoVboLst.size() != 4){
        throw std::length_error("Illegal state exeption, vaoVboLst size != 4");
    }
    // speed time
    std::function<glm::mat4(double,double)> fcTronc = [](float speed, float time){
        return glm::mat4(1);
    };
    std::function<glm::mat4(double,double)> fcFeuille = [](float speed, float time){
        glm::mat4 mat(1);
        mat = glm::translate(mat ,glm::vec3(0, 2,0));// pos init
        //mat = glm::translate(mat ,glm::vec3(cos(time*speed)/10., 0,0));// movement des feuille
        return mat;
    };
    apple.push_back(std::make_tuple(vaoVboLst[0] ,vertexCount[0], fcFeuille));
    apple.push_back(std::make_tuple(vaoVboLst[1] ,vertexCount[1], fcTronc));

    return apple;
}

// opti memoire precalcule dse bloque


std::vector<GLuint> bindTextureApple(int VERTEX_ATTR_POSITION, int VERTEX_ATTR_NORMAL, int VERTEX_ATTR_TEXCOORDS, int VERTEX_ATTR_TEXTURE){
  std::vector<GLuint> vaoVboLst;
    //// Feuille
    GLuint vboDecoFeuille;
    glGenBuffers(1, &vboDecoFeuille);
    glBindBuffer(GL_ARRAY_BUFFER,vboDecoFeuille);
    // ajout des point du tr
    Cube feuille(53,53,53); // Front Up Down
    //Cube feuille(0,0,0); // Front Up Down
    // ligne de 9 float
    /************ Calcul feuille *************/

    glm::mat4 mat(1);
    mat = glm::translate(mat ,glm::vec3(0, 6,0));// pos init
    std::vector<float> data;
    fuseData(data,feuille.getDataPointer(), feuille.getVertexCount(),mat);
    for (auto x = -1; x< 2; x++){
        for (auto z = -1; z< 2; z++){
            mat = glm::translate(glm::mat4(1) ,glm::vec3(2*x, 2*2,2*z));// pos init
            fuseData(data,feuille.getDataPointer(), feuille.getVertexCount(),mat);
        }    
    }
    for (auto x = -1; x< 2; x++){
        for (auto z = -1; z< 2; z++){

            if (x!=0 || z !=0){
                mat = glm::translate(glm::mat4(1) ,glm::vec3(2*x, 2*1,2*z));// pos init
                fuseData(data,feuille.getDataPointer(), feuille.getVertexCount(),mat);
            }
        }    
    }


    /*************************/

    glBufferData(GL_ARRAY_BUFFER, (8 + 9 +1)*feuille.getVertexCount()*sizeof(ShapeVertex) , data.data(),GL_STATIC_DRAW);
    //glBufferData(GL_ARRAY_BUFFER, feuille.getVertexCount()*sizeof(ShapeVertex) , feuille.getDataPointer(),GL_STATIC_DRAW);
    // debind les info
    glBindBuffer(GL_ARRAY_BUFFER,0);
    // description / interpretation des flotant obtenu dans le gpu
    GLuint vaoDecoFeuille;
    glGenVertexArrays(1,&vaoDecoFeuille);
    glBindVertexArray(vaoDecoFeuille);
    glEnableVertexAttribArray(VERTEX_ATTR_POSITION);
    glEnableVertexAttribArray(VERTEX_ATTR_NORMAL);
    glEnableVertexAttribArray(VERTEX_ATTR_TEXCOORDS);
    glEnableVertexAttribArray(VERTEX_ATTR_TEXTURE);

    // Lier vbo et vao
    glBindBuffer(GL_ARRAY_BUFFER,vboDecoFeuille);
    glVertexAttribPointer(VERTEX_ATTR_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), 0);
    glVertexAttribPointer(VERTEX_ATTR_NORMAL, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)(sizeof(glm::vec3)));
    glVertexAttribPointer(VERTEX_ATTR_TEXCOORDS, 2, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)(sizeof(glm::vec3)+sizeof(glm::vec3)));
    glVertexAttribPointer(VERTEX_ATTR_TEXTURE, 1, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)(sizeof(glm::vec3)+sizeof(glm::vec3)+sizeof(glm::vec2)));
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindVertexArray(0); 

    ///////////////////////////////////////////////////////////////////////////////////////
    // Tronc
       GLuint vboDecoTronc;
    glGenBuffers(1, &vboDecoTronc);
    glBindBuffer(GL_ARRAY_BUFFER,vboDecoTronc);
    // ajout des point du tr
    Cube tronc(20,21,21); // Front Up Down
    //Cube feuille(0,0,0); // Front Up Down
    // ligne de 9 float
    /************ Calcul Tronc *************/
    data.clear();
    mat = glm::mat4(1);
    fuseData(data,tronc.getDataPointer(), tronc.getVertexCount(),mat);
    mat = glm::translate(mat ,glm::vec3(0, 2,0));// pos init
    fuseData(data,tronc.getDataPointer(), tronc.getVertexCount(),mat);
    mat = glm::translate(mat ,glm::vec3(0, 2,0));// pos init
    fuseData(data,tronc.getDataPointer(), tronc.getVertexCount(),mat);


    /*************************/

    glBufferData(GL_ARRAY_BUFFER, 3*tronc.getVertexCount()*sizeof(ShapeVertex) , data.data(),GL_STATIC_DRAW);
    //glBufferData(GL_ARRAY_BUFFER, feuille.getVertexCount()*sizeof(ShapeVertex) , feuille.getDataPointer(),GL_STATIC_DRAW);
    // debind les info
    glBindBuffer(GL_ARRAY_BUFFER,0);
    // description / interpretation des flotant obtenu dans le gpu
    GLuint vaoDecoTronc;
    glGenVertexArrays(1,&vaoDecoTronc);
    glBindVertexArray(vaoDecoTronc);
    glEnableVertexAttribArray(VERTEX_ATTR_POSITION);
    glEnableVertexAttribArray(VERTEX_ATTR_NORMAL);
    glEnableVertexAttribArray(VERTEX_ATTR_TEXCOORDS);
    glEnableVertexAttribArray(VERTEX_ATTR_TEXTURE);

    // Lier vbo et vao
    glBindBuffer(GL_ARRAY_BUFFER,vboDecoTronc);
    glVertexAttribPointer(VERTEX_ATTR_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), 0);
    glVertexAttribPointer(VERTEX_ATTR_NORMAL, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)(sizeof(glm::vec3)));
    glVertexAttribPointer(VERTEX_ATTR_TEXCOORDS, 2, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)(sizeof(glm::vec3)+sizeof(glm::vec3)));
    glVertexAttribPointer(VERTEX_ATTR_TEXTURE, 1, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)(sizeof(glm::vec3)+sizeof(glm::vec3)+sizeof(glm::vec2)));
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindVertexArray(0); 

  vaoVboLst.push_back(vaoDecoFeuille);
  vaoVboLst.push_back(vaoDecoTronc);
  vaoVboLst.push_back(vboDecoFeuille);
  vaoVboLst.push_back(vboDecoTronc);
  return vaoVboLst;
}

std::vector<int> getVertexCountApple(){
    Cube Feuille(53,53,53); // Front Up Down
    Cube Tronc(20,21,21); // Front Up Down
    std::vector<int> vertexCount;
    vertexCount.push_back((8+9+1)*Feuille.getVertexCount());
    vertexCount.push_back(3*Tronc.getVertexCount());
    return vertexCount;
}

/****************************************************************  Sapin  ****************************************************************/
std::vector<std::tuple<GLuint, int, std::function<glm::mat4(double,double)>>>
createSapin(const std::vector<GLuint> &vaoVboLst,const std::vector<int> &vertexCount){
    std::vector<std::tuple<GLuint, int, std::function<glm::mat4(double,double)>>> apple;
    if (vaoVboLst.size() != 4){
        throw std::length_error("Illegal state exeption, vaoVboLst size != 4");
    }
    // speed time
    std::function<glm::mat4(double,double)> fcTronc = [](float speed, float time){
        return glm::mat4(1);
    };
    std::function<glm::mat4(double,double)> fcFeuille = [](float speed, float time){
        glm::mat4 mat(1);
        mat = glm::translate(mat ,glm::vec3(0, 4,0));// pos init
        //mat = glm::translate(mat ,glm::vec3(cos(time*speed)/10., 0,0));// movement des feuille
        return mat;
    };
    apple.push_back(std::make_tuple(vaoVboLst[0] ,vertexCount[0], fcFeuille));
    apple.push_back(std::make_tuple(vaoVboLst[1] ,vertexCount[1], fcTronc));

    return apple;
}

// opti memoire precalcule dse bloque


std::vector<GLuint> bindTextureSapin(int VERTEX_ATTR_POSITION, int VERTEX_ATTR_NORMAL, int VERTEX_ATTR_TEXCOORDS, int VERTEX_ATTR_TEXTURE){
  std::vector<GLuint> vaoVboLst;
    //// Feuille
    GLuint vboDecoFeuille;
    glGenBuffers(1, &vboDecoFeuille);
    glBindBuffer(GL_ARRAY_BUFFER,vboDecoFeuille);
    // ajout des point du tr
    Cube feuille(133,133,133); // Front Up Down
    //Cube feuille(0,0,0); // Front Up Down
    // ligne de 9 float
    /************ Calcul feuille *************/

    glm::mat4 mat(1);
    mat = glm::translate(mat ,glm::vec3(0, 6,0));// pos init
    std::vector<float> data;
    fuseData(data,feuille.getDataPointer(), feuille.getVertexCount(),mat);
    for (auto x = -1; x< 2; x++){
        for (auto z = -1; z< 2; z++){
            mat = glm::translate(glm::mat4(1) ,glm::vec3(2*x, 2*2,2*z));// pos init
            fuseData(data,feuille.getDataPointer(), feuille.getVertexCount(),mat);
        }    
    }
    for (auto x = -2; x< 3; x++){
        for (auto z = -2; z< 3; z++){

            if ((x!=0 || z !=0)){
                if ((x != -2 || z != -2) && (x != -2 || z != 2) && (x != 2 || z != 2) && (x != 2 || z != -2)){
                    mat = glm::translate(glm::mat4(1) ,glm::vec3(2*x, 2*1,2*z));// pos init
                    fuseData(data,feuille.getDataPointer(), feuille.getVertexCount(),mat);                    
                }
            }
        }    
    }


    /*************************/


    glBufferData(GL_ARRAY_BUFFER, (20 + 9 +1)*feuille.getVertexCount()*sizeof(ShapeVertex) , data.data(),GL_STATIC_DRAW);
    //glBufferData(GL_ARRAY_BUFFER, feuille.getVertexCount()*sizeof(ShapeVertex) , feuille.getDataPointer(),GL_STATIC_DRAW);
    // debind les info
    glBindBuffer(GL_ARRAY_BUFFER,0);
    // description / interpretation des flotant obtenu dans le gpu
    GLuint vaoDecoFeuille;
    glGenVertexArrays(1,&vaoDecoFeuille);
    glBindVertexArray(vaoDecoFeuille);
    glEnableVertexAttribArray(VERTEX_ATTR_POSITION);
    glEnableVertexAttribArray(VERTEX_ATTR_NORMAL);
    glEnableVertexAttribArray(VERTEX_ATTR_TEXCOORDS);
    glEnableVertexAttribArray(VERTEX_ATTR_TEXTURE);

    // Lier vbo et vao
    glBindBuffer(GL_ARRAY_BUFFER,vboDecoFeuille);
    glVertexAttribPointer(VERTEX_ATTR_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), 0);
    glVertexAttribPointer(VERTEX_ATTR_NORMAL, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)(sizeof(glm::vec3)));
    glVertexAttribPointer(VERTEX_ATTR_TEXCOORDS, 2, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)(sizeof(glm::vec3)+sizeof(glm::vec3)));
    glVertexAttribPointer(VERTEX_ATTR_TEXTURE, 1, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)(sizeof(glm::vec3)+sizeof(glm::vec3)+sizeof(glm::vec2)));
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindVertexArray(0); 

    ///////////////////////////////////////////////////////////////////////////////////////
    // Tronc
       GLuint vboDecoTronc;
    glGenBuffers(1, &vboDecoTronc);
    glBindBuffer(GL_ARRAY_BUFFER,vboDecoTronc);
    // ajout des point du tr
    Cube tronc(116,21,21); // Front Up Down
    //Cube feuille(0,0,0); // Front Up Down
    // ligne de 9 float
    /************ Calcul Tronc *************/
    data.clear();
    mat = glm::mat4(1);
    fuseData(data,tronc.getDataPointer(), tronc.getVertexCount(),mat);
    mat = glm::translate(mat ,glm::vec3(0, 2,0));// pos init
    fuseData(data,tronc.getDataPointer(), tronc.getVertexCount(),mat);
    mat = glm::translate(mat ,glm::vec3(0, 2,0));// pos init
    fuseData(data,tronc.getDataPointer(), tronc.getVertexCount(),mat);
    mat = glm::translate(mat ,glm::vec3(0, 2,0));// pos init
    fuseData(data,tronc.getDataPointer(), tronc.getVertexCount(),mat);


    /*************************/

    glBufferData(GL_ARRAY_BUFFER, 3*tronc.getVertexCount()*sizeof(ShapeVertex) , data.data(),GL_STATIC_DRAW);
    //glBufferData(GL_ARRAY_BUFFER, feuille.getVertexCount()*sizeof(ShapeVertex) , feuille.getDataPointer(),GL_STATIC_DRAW);
    // debind les info
    glBindBuffer(GL_ARRAY_BUFFER,0);
    // description / interpretation des flotant obtenu dans le gpu
    GLuint vaoDecoTronc;
    glGenVertexArrays(1,&vaoDecoTronc);
    glBindVertexArray(vaoDecoTronc);
    glEnableVertexAttribArray(VERTEX_ATTR_POSITION);
    glEnableVertexAttribArray(VERTEX_ATTR_NORMAL);
    glEnableVertexAttribArray(VERTEX_ATTR_TEXCOORDS);
    glEnableVertexAttribArray(VERTEX_ATTR_TEXTURE);

    // Lier vbo et vao
    glBindBuffer(GL_ARRAY_BUFFER,vboDecoTronc);
    glVertexAttribPointer(VERTEX_ATTR_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), 0);
    glVertexAttribPointer(VERTEX_ATTR_NORMAL, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)(sizeof(glm::vec3)));
    glVertexAttribPointer(VERTEX_ATTR_TEXCOORDS, 2, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)(sizeof(glm::vec3)+sizeof(glm::vec3)));
    glVertexAttribPointer(VERTEX_ATTR_TEXTURE, 1, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)(sizeof(glm::vec3)+sizeof(glm::vec3)+sizeof(glm::vec2)));
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindVertexArray(0); 

  vaoVboLst.push_back(vaoDecoFeuille);
  vaoVboLst.push_back(vaoDecoTronc);
  vaoVboLst.push_back(vboDecoFeuille);
  vaoVboLst.push_back(vboDecoTronc);
  return vaoVboLst;
}

std::vector<int> getVertexCountSapin(){
    Cube Feuille(116,116,116); // Front Up Down
    Cube Tronc(132,21,21); // Front Up Down
    std::vector<int> vertexCount;
    vertexCount.push_back((20+9+1)*Feuille.getVertexCount());
    vertexCount.push_back(3*Tronc.getVertexCount());
    return vertexCount;
}



/****************************************************************  Map  ****************************************************************/



std::vector<int> initHeightMap() {
    auto heightMapImg = loadImage("../../assets/textures/hmp.bmp");
    if(!heightMapImg) {
        std::cout << "image is not loaded" << std::endl;
    }
    return loadHeightMap(std::move(heightMapImg));
}   

std::vector<std::tuple<GLuint, int, std::function<glm::mat4(double,double)>>>
createMap(const std::vector<GLuint> &vaoVboLst,const std::vector<int> &vertexCount){
    if (vaoVboLst.size() != 2){
        throw std::length_error("Illegal state exeption, vaoVboLst size != 4");
    }
    std::vector<std::tuple<GLuint, int, std::function<glm::mat4(double,double)>>> map;
    // speed time
    std::function<glm::mat4(double,double)> fcBody = [](float speed, float time){
        return glm::mat4(1);
    };
    map.push_back(std::make_tuple(vaoVboLst[0] ,vertexCount[0], fcBody));
    return map;
}

// opti memoire precalcule dse bloques
// Block 24 * 24
std::vector<GLuint> bindTextureMap(int VERTEX_ATTR_POSITION, int VERTEX_ATTR_NORMAL, int VERTEX_ATTR_TEXCOORDS, int VERTEX_ATTR_TEXTURE){
  std::vector<GLuint> vaoVboLst;
    //// Feuille
    GLuint vboDecoMap;
    glGenBuffers(1, &vboDecoMap);
    glBindBuffer(GL_ARRAY_BUFFER,vboDecoMap);
    Cube dirt(3,0,2); // Front Up Down
    /************ Calcul map *************/
    auto heightMap = initHeightMap();
    glm::mat4 mat(1);
    mat = glm::translate(mat ,glm::vec3(0, 6,0));// pos init

    int DIM = 25;
    int nb_block = ((DIM*DIM*DIM))*dirt.getVertexCount()*9; // nb tt de float
    float* data = new float[nb_block];
    int posData = 0;
     int index = 0;
     for(int i = 0; i < DIM; i++) {
        for(int j = 0; j < DIM; j++) {
            for(int k = 0; k < DIM; k++) {
                auto tmp = glm::translate(glm::mat4(1) ,glm::vec3(i*2, k*2, j*2));
                fuseBigData(data,posData ,dirt.getDataPointer(), dirt.getVertexCount(),tmp);
                posData += dirt.getVertexCount()*9;
            }
        }
     }
        // Taille max vbo = 179 * 32
    /*************************/
    std::cout << "Nb block " << nb_block << std::endl;
    std::cout << "Nb block " << nb_block << std::endl;
    glBufferData(GL_ARRAY_BUFFER, posData* sizeof(float) , data,GL_STATIC_DRAW);
    //glBufferData(GL_ARRAY_BUFFER, map.getVertexCount()*sizeof(ShapeVertex) , map.getDataPointer(),GL_STATIC_DRAW);
    // debind les info
    glBindBuffer(GL_ARRAY_BUFFER,0);
    // description / interpretation des flotant obtenu dans le gpu
    GLuint vaoDecoMap;
    glGenVertexArrays(1,&vaoDecoMap);
    glBindVertexArray(vaoDecoMap);
    glEnableVertexAttribArray(VERTEX_ATTR_POSITION);
    glEnableVertexAttribArray(VERTEX_ATTR_NORMAL);
    glEnableVertexAttribArray(VERTEX_ATTR_TEXCOORDS);
    glEnableVertexAttribArray(VERTEX_ATTR_TEXTURE);

    // Lier vbo et vao
    glBindBuffer(GL_ARRAY_BUFFER,vboDecoMap);
    glVertexAttribPointer(VERTEX_ATTR_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), 0);
    glVertexAttribPointer(VERTEX_ATTR_NORMAL, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)(sizeof(glm::vec3)));
    glVertexAttribPointer(VERTEX_ATTR_TEXCOORDS, 2, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)(sizeof(glm::vec3)+sizeof(glm::vec3)));
    glVertexAttribPointer(VERTEX_ATTR_TEXTURE, 1, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)(sizeof(glm::vec3)+sizeof(glm::vec3)+sizeof(glm::vec2)));
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindVertexArray(0); 


    //delete data;
   

  vaoVboLst.push_back(vaoDecoMap);
  vaoVboLst.push_back(vboDecoMap);
  return vaoVboLst;
}

std::vector<int> getVertexCountMap(){

    std::vector<int> vertexCount;

    Cube dirt(116,116,116); // Front Up Down

    auto heightMap = initHeightMap();
     int DIM = 25;
    int nb_block = ((DIM*DIM*DIM))*dirt.getVertexCount();
    

     std::cout << "Nb vertew" << nb_block << std::endl;
    vertexCount.push_back(nb_block);
    return vertexCount;
}
}