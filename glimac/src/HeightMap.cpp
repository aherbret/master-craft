#include "glimac/HeightMap.hpp"

std::vector<int> loadHeightMap(std::unique_ptr<Image> heightMapImg) {
    auto img = heightMapImg.get()->getPixels();
    std::vector<int> heightMap = std::vector<int>();
    for(int i = 0; i < heightMapImg->getHeight() * heightMapImg->getWidth(); i++) {
        //int cubes_nb = img[i][0] * 255.;
        int cube_height = img[i][0] * 256;
        //std::cout << cube_height << std::endl;
        heightMap.push_back(cube_height);
    }
    return heightMap;
}

Cube cubeType(int colorx, int colory, int colorz) {
    if(colorx == 34 && colory == 177 && colorz == 76) {
        // grass
        return Cube(3, 0, 2);
    } else if(colorx == 255 && colory == 242 && colorz == 0) {
        // sand
        return Cube(4, 1, 3);
    } else if(colorx == 185 && colory == 122 && colorz == 87) {
        // mountain
        return Cube(5, 2, 4);
    } else if(colorx == 0 && colory == 162 && colorz == 232) {
        // water
        return Cube(6, 3, 5);
    } else {
        throw "Unknown color type!";
    }
}
/*
std::vector<Cube> loadColoredHeightMap(std::unique_ptr<Image> coloredHeightMapImg) {
    auto img = coloredHeightMapImg.get()->getPixels();
    std::vector<Cube> coloredHeightMap = std::vector<Cube>();

    for(int i = 0; i < coloredHeightMapImg->getHeight() * coloredHeightMapImg->getWidth(); i++) {
        int colorx = img[i][0] * 255;
        int colory = img[i][1] * 255;
        int colorz = img[i][2] * 255;
        coloredHeightMap.push_back(cubeType(colorx, colory, colorz));
    }

    return coloredHeightMap;   
}*/

std::vector<glm::vec3> loadColoredMap(std::unique_ptr<Image> coloredHeightMapImg) {
    auto img = coloredHeightMapImg.get()->getPixels();
    std::vector<glm::vec3> lstVecColor;
    for(int i = 0; i < coloredHeightMapImg->getHeight() * coloredHeightMapImg->getWidth(); i++) {
        int colorx = img[i][0] * 255;
        int colory = img[i][1] * 255;
        int colorz = img[i][2] * 255;
    lstVecColor.push_back(glm::vec3(colorx,colory,colorz));
    }
    return lstVecColor;   
}


std::vector<int> initHeightMap() {
    auto heightMapImg = loadImage("../../assets/textures/hMap.jpg");
    if(!heightMapImg) {
        std::cout << "image is not loaded" << std::endl;
    }
    return loadHeightMap(std::move(heightMapImg));
}
