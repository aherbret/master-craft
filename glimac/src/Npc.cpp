#include "glimac/Npc.hpp"

namespace glimac {

// subNpc            
void subNpc::setSpeed(float speed, float speedAng){
    speed = speed;
    speedAng = speedAng;                
}
void subNpc::reset(){
    cibleNpc = positionNpc;
    cibleNpcIntermediaire = positionNpc;
}

//Npc

void Npc::uDraw(float time,
                std::shared_ptr<std::map<std::string, GLint>> uMap,
                const glm::vec3 &posCam,
                const glm::mat4 &ProjMatrix,
                const glm::mat4 &MVMatrix, 
                const glm::mat4 &NormalMatrix, 
                LightSource &lightSrc,
                subNpc &npcPos
            ){
                if (!hasAI && !hasForm)
                    throw std::logic_error("Mush init AI && Form");
                form->uDraw(time, uMap, posCam, ProjMatrix, MVMatrix, NormalMatrix, npcPos.deplacement,lightSrc);
            }
            

bool Npc::updateCible(subNpc &npcPos, const DeepMap& deepMap){
                if (glm::distance(npcPos.positionNpc,npcPos.cibleNpc) < npcPos.speed*2){
                    npcPos.cibleNpc = generateCible(deepMap, npcPos.positionNpc);
                    npcPos.cibleNpcIntermediaire = npcPos.cibleNpc;
                }
                else if (glm::distance(npcPos.positionNpc,npcPos.cibleNpcIntermediaire) < npcPos.speed*2){
                    npcPos.cibleNpcIntermediaire = npcPos.cibleNpc;
                }
                auto tmpPos = nextPosGenerate(npcPos);
                /*Test si pos possible*/
                // En temp normal on calcule toutes les case intermediare pour la position cible mais comme la speed ne peut pas dépasser 1 il n'y a pas ce pb
                //std::cout << " A " << npcPos.cibleNpcIntermediaire.x << " " << npcPos.cibleNpcIntermediaire.z << " " << std::endl;
                //std::cout << " B " << tmpPos.x << " " << tmpPos.z << " " << std::endl;
                try{
                    if (!deepMap.isEmpty(tmpPos.x,tmpPos.y,tmpPos.z)){
                        //std::cerr << " Colision " << tmpPos <<  "Cible " << npcPos.cibleNpc <<std::endl;
                        npcPos.cibleNpcIntermediaire = generateDodgeAI(deepMap, npcPos.positionNpc, npcPos.cibleNpcIntermediaire);
                    }
                }
                catch(...){
                    //std::cerr << "Warning !! position out of the map" << std::endl;
                    // Plus dans la map 
                }


                npcPos.deplacement = deplacementGeneral(npcPos);
                
            }

void Npc::setElementForm(float speed,
    GLuint texture,
    LightBlock *lightMaterial,
    std::function<std::vector<GLuint>(int,int,int,int)> bind,
    std::function<std::vector<std::tuple<GLuint, int, std::function<glm::mat4(double,double)>>>(const std::vector<GLuint>,const std::vector<int>)>draw,
    std::function<std::vector<int>()>getVertexCount,
    int VERTEX_ATTR_POSITION,
    int VERTEX_ATTR_NORMAL,
    int VERTEX_ATTR_TEXCOORDS,
    int VERTEX_ATTR_TEXTURE)
{
    form = ElementForm::CreateElementForm(speed, texture, lightMaterial, bind, draw, getVertexCount, VERTEX_ATTR_POSITION, VERTEX_ATTR_NORMAL, VERTEX_ATTR_TEXCOORDS, VERTEX_ATTR_TEXTURE);
    hasForm = true;
}

}